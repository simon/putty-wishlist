#!/usr/bin/env python3

import sys
import os
import re
import collections
import subprocess
import argparse
import contextlib
import html
import fnmatch
import traceback
import textwrap
import shutil
import tempfile
import html.parser
import itertools
import string

import gitrepo
import gitreach
import extraparents

@contextlib.contextmanager
def nullcontext(arg):
    yield arg

bug_classes = {
    'vulnerability': "This is a security vulnerability.",
    'bug': "This is clearly an actual problem we want fixed.",
    'semi-bug': ("This might or might not be a bug, depending on your "+
                 "precise definition of what a bug is."),
    'wish': "This is a request for an enhancement.",
}

bug_priorities = {
    'high': "This should be fixed in the next release.",
    'medium': "This should be fixed one day.",
    'low': "We aren't sure whether to fix this or not.",
    'dormant': "This is an old feature request that we no longer think is relevant.",
    'historic': "This is an old bug report that we think is either fixed without noticing, or confined to old systems, or too vague.",
    'never': "We don't ever intend to fix this.",
}

# Difficulty levels have to be an OrderedDict so that we can enumerate
# them in the right order during index.html output.
bug_difficulties = collections.OrderedDict([
    ('fun', "Just needs tuits, and not many of them."),
    ('tricky', "Needs many tuits."),
    ('taxing', "Needs external things we don't have (standards, users etc)"),
    ('mayhem', "Probably impossible"),
])

bug_stylesheet = """\
table { border-collapse: collapse; }
td, th { border: 1px solid black; padding: 0.2em; }
th { text-align: left; }
"""

# Issues fixed in this release or earlier are relegated to the
# "historic" wishlist page.
LAST_HISTORIC_RELEASE = "0.78"

class BugFormatError(Exception): pass

class Version(object):
    snapdate_re = re.compile(r'^(\d{4})-(\d{2})-(\d{2})$')
    svnrev_re = re.compile(r'^r(\d+)$')
    release_re = re.compile(r'^(\d)\.(\d{2})(b?)$')
    gitcommit_re = re.compile(r'^(~?)([0-9a-f]{7,40})$')
    def __init__(self, string):
        self.value = None
        self.repr = "Version(%s)" % repr(string)

        m = self.snapdate_re.match(string)
        if m:
            assert self.value is None
            self.value = ("snapdate", int(m.group(1)),
                          int(m.group(2)), int(m.group(3)))

        m = self.svnrev_re.match(string)
        if m:
            if self.value is not None:
                raise BugFormatError("ambiguous version '%s'" % string)
            self.value = ("svnrev", int(m.group(1)))

        m = self.release_re.match(string)
        if m:
            if self.value is not None:
                raise BugFormatError("ambiguous version '%s'" % string)
            self.value = ("release", int(m.group(1)), int(m.group(2)),
                          m.group(3))

        m = self.gitcommit_re.match(string)
        if m:
            if self.value is not None:
                raise BugFormatError("ambiguous version '%s'" % string)
            # Third element of the tuple is True, unless the commit id
            # was prefixed with ~, in which case it's False. The
            # latter indicates that this commit id is 'approximate',
            # usually because we have reason to think that the bug was
            # gone by a particular point in time but aren't sure which
            # commit (at or before that point) actually made it go
            # away.
            self.value = ("gitcommit", m.group(2), m.group(1) != "~")

        if self.value is None:
            raise BugFormatError("unrecognised version '%s'" % string)

        self.string = string

        self.inferred = False

    def inferred_copy(self):
        ret = Version(self.string)
        ret.inferred = True
        return ret

    def __str__(self):
        return self.string

    def __repr__(self):
        return self.repr

    def __hash__(self):
        return hash(self.value)

    def vertype(self):
        return self.value[0]

    def same_type(self, rhs):
        return self.value[0] == rhs.value[0]

    def githash(self):
        assert self.value[0] == "gitcommit"
        return self.value[1]

    def __eq__(self, rhs):
        return self.value == rhs.value

    def __cmp__(self, rhs):
        assert self.same_type(rhs)

        # can't compare git commit ids without reference to the repository
        assert self.value[0] != "gitcommit"

        return (-1 if self.value < rhs.value else
                +1 if self.value > rhs.value else 0)

    def __lt__(self, rhs): return self.__cmp__(rhs) < 0
    def __le__(self, rhs): return self.__cmp__(rhs) <= 0
    def __gt__(self, rhs): return self.__cmp__(rhs) > 0
    def __ge__(self, rhs): return self.__cmp__(rhs) >= 0

    def text_description(self, release_vers):
        assert self.value[0] == "release"
        return ("current snapshots" if self == self.nextrel() else
                "pre-release builds of %s" % self if release_vers[self] else
                "release %s" % self)

    def anchor_fragment(self, release_vers):
        assert self.value[0] == "release"
        return ("snapshots" if self == self.nextrel() else
                "prerelease-%s" % self if release_vers[self] else
                "release-%s" % self)

    @classmethod
    def dawnoftime(cls):
        ret = cls("0.00")
        ret.string = None # protect against accidentally outputting this
        ret.value = ("release", float("-inf"))
        ret.repr = "Version.dawnoftime()"
        return ret

    @classmethod
    def nextrel(cls):
        ret = cls("0.00")
        ret.string = None # protect against accidentally outputting this
        ret.value = ("release", float("inf"))
        ret.repr = "Version.nextrel()"
        return ret

LAST_HISTORIC_RELEASE = Version(LAST_HISTORIC_RELEASE)

def cve_url(cveid):
    return "https://cve.mitre.org/cgi-bin/cvename.cgi?name=" + cveid

def rfc_url(rfcno):
    return "https://www.rfc-editor.org/rfc/rfc" + rfcno

class HTMLPreprocessor(html.parser.HTMLParser):
    def __init__(self, allbugs, commit_link):
        super().__init__(convert_charrefs=False)
        self.outdata = ""
        self.allbugs = allbugs
        self.commit_link = commit_link
        self.cveids = set()

    def handle_starttag(self, tag, attrs):
        attrdict = dict(attrs)
        if tag == "bug":
            try:
                bugid = attrdict["id"]
            except KeyError:
                raise BugFormatError("bug link without id")
            fragment = attrdict.get("fragment", "")
            if fragment != "" and not fragment.startswith("#"):
                raise BugFormatError("bug link fragment '%s' does not "
                                     "start with '#'" % fragment)
            self.outdata += '<a href="' + bugid + '.html' + fragment + '">'
            self.default_link_text = bugid
        elif tag == "cve":
            try:
                cveid = attrdict["id"]
            except KeyError:
                raise BugFormatError("CVE link without id")
            self.outdata += '<a href="{}">'.format(html.escape(cve_url(cveid)))
            self.default_link_text = html.escape(cveid)
            if attrdict.get("index", "yes").lower() != "no":
                self.cveids.add(cveid)
        elif tag == "gitcommit":
            try:
                githash = attrdict["hash"]
            except KeyError:
                raise BugFormatError("gitcommit tag without hash")
            if not (len(githash) == 40 and
                    githash.rstrip("0123456789abcdef") == ""):
                raise BugFormatError("gitcommit tag with malformed hash")
            self.outdata += '<a href="{}">'.format(
                html.escape(self.commit_link.replace("{COMMIT}", githash)))
            self.default_link_text = githash
        elif tag == "rfc":
            try:
                rfcno = attrdict["id"]
            except KeyError:
                raise BugFormatError("RFC link without id")
            fragment = attrdict.get("fragment", "")
            if fragment != "" and not fragment.startswith("#"):
                raise BugFormatError("RFC link fragment '%s' does not "
                                     "start with '#'" % fragment)
            rfcurl = html.escape(rfc_url(rfcno)) + fragment
            self.outdata += '<a href="{}">'.format(rfcurl)
            self.default_link_text = html.escape("RFC " + rfcno)
        else:
            self.outdata += self.get_starttag_text()

    def handle_endtag(self, tag):
        if tag in ["bug", "cve", "gitcommit", "rfc"]:
            self.outdata += self.default_link_text + '</a>'
        else:
            self.outdata += '</' + tag + '>'

    def handle_data(self, data):
        self.outdata += data
        self.default_link_text = ""
    def handle_entityref(self, name):
        self.outdata += '&' + name + ';'
    def handle_charref(self, num):
        self.outdata += '&#' + num + ';'

class Bug(object):
    def __init__(self, bugname, fileobj=None, filename=None):
        self.bugname = bugname
        self.last_modified = None # default for bugs not under version control

        headers = {}
        def header(hdr, optional=False, postprocess=lambda x:x,
                   default=None, valid=None):
            key = hdr.lower()
            if key in headers:
                ret = headers[key]
                if valid is not None and ret not in valid:
                    raise BugFormatError("bad value for '%s': '%s'" % (
                        hdr, ret))
                del headers[key]
                return postprocess(ret)
            elif optional:
                return default
            else:
                raise BugFormatError("missing header '%s'" % hdr)
        def optheader(hdr, **kws): return header(hdr, optional=True, **kws)
        def listheader(hdr, **kws):
            return header(hdr, optional=True, default=[],
                          postprocess=lambda s: s.split(" "), **kws)

        context = (nullcontext(fileobj) if fileobj is not None
                   else open(filename))
        with context as f:
            for line in iter(f.readline, "\n"):
                line = line.rstrip("\r\n")
                hdr, value = line.split(":", 1)
                value = value.lstrip(" \t")
                hdr = hdr.lower()
                headers[hdr] = value
            self.Body = f.read()

        self.Class = optheader('Class', valid=bug_classes)
        self.Priority = optheader('Priority', valid=bug_priorities)
        self.Difficulty = optheader('Difficulty', valid=bug_difficulties)
        self.Summary = header('Summary')
        self.Content_Type = header('Content-Type',
                                   valid=['text/plain', 'text/x-html-body'])
        self.Fixed_In = list(map(Version, listheader('Fixed-in')))
        self.Present_In = list(map(Version, listheader('Present-in')))
        self.Absent_In = list(map(Version, listheader('Absent-in')))
        self.Depends = listheader('Depends')
        if len(headers) > 0:
            raise BugFormatError("unknown headers: " +
                                 ",".join(sorted(headers)))

        self.cveids = set() # to_html will populate this as a side effect

    def to_html(self, commit_link, allbugs):
        classname = self.Class if self.Class is not None else "artifact"
        ret = ""
        ret += textwrap.dedent("""\
        <html><head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>
        <title>PuTTY {classname} {bugname}</title>
        <style type="text/css">{stylesheet}</style>
        </head><body>
        <h1 align=center>PuTTY {classname} {bugname}</h1>
        <a name="endheader"></a>
        """).format(
            classname=classname,
            bugname=html.escape(self.bugname),
            stylesheet=bug_stylesheet,
        )
        ret += "<b>summary</b>: %s<br>\n" % html.escape(self.Summary)

        def keywordhdr(name, value, descriptions):
            if value is not None:
                return "<b>%s</b>: <i>%s:</i> %s<br>\n" % (
                    name, value, html.escape(descriptions[value]))
            else:
                return ""
        def listhdr_general(name, values):
            if len(values) > 0:
                return "<b>%s</b>: %s<br>\n" % (name, " ".join(values))
            else:
                return ""
        def listhdr_bugs(name, bugs):
            links = []
            for b in bugs:
                link = '<a href="%s.html">%s</a>' % (html.escape(b.bugname),
                                                     html.escape(b.bugname))
                # Emphasise bugs if they are unfixed.
                if len(b.Fixed_In) == 0:
                    link = '<em>%s</em>' % link
                links.append(link)
            return listhdr_general(name, links)
        def listhdr_versions(name, versions):
            vers = []
            for v in versions:
                ver = html.escape(str(v))
                # Parenthesise versions if they are inferred rather
                # than explicit in the input.
                if v.inferred:
                    ver = "(%s)" % ver
                # Augment git commits with a helpful link to the
                # commit in the project's gitweb.
                if v.vertype() == "gitcommit":
                    githash = v.githash()
                    ver = ver.replace(githash, '<a href="%s">%s</a>' % (
                            html.escape(commit_link.replace("{COMMIT}",
                                                           githash)),
                            html.escape(githash)))
                vers.append(ver)
            return listhdr_general(name, vers)

        ret += keywordhdr("class", self.Class, bug_classes)
        ret += keywordhdr("difficulty", self.Difficulty, bug_difficulties)
        ret += listhdr_bugs("depends", self.Depends)
        ret += listhdr_bugs("blocks", self.Blocks)
        ret += keywordhdr("priority", self.Priority, bug_priorities)
        ret += listhdr_versions("absent-in", self.Absent_In)
        ret += listhdr_versions("present-in", self.Present_In)
        ret += listhdr_versions("fixed-in", self.Fixed_In)

        if self.Content_Type == "text/x-html-body":
            try:
                preproc = HTMLPreprocessor(allbugs, commit_link)
                preproc.feed(self.Body)
                preproc.close()
                self.cveids = preproc.cveids
            except BugFormatError as e:
                raise BugFormatError(self.bugname + ": " + str(e))
            ret += preproc.outdata
        else:
            ret += "<p><pre>%s</pre>" % html.escape(self.Body)
            self.cveids = set()
        ret += ('<div class="audit"><a href="%s">Audit trail</a>'
                ' for this %s.</div>\n') % (
            html.escape(self.audit_url),
            classname,
        )
        if self.last_modified is not None:
            ret += ('<div class="timestamp">(last revision of this bug'
                    ' record was at %s)</div>\n') % (
                        html.escape(self.last_modified),
                    )
        else:
            ret += ('<div class="timestamp">(this bug record is not yet'
                    ' in version control)</div>\n')
        ret += "</body></html>\n"
        return ret

    @property
    def obsolete(self):
        if self.first_fixed_rel is None:
            # An unfixed thing counts as obsolete (and hence is
            # relegated to another page) if it has priority 'dormant'
            # or 'historic'.
            if self.Priority in {"dormant", "historic"}:
                return True
        else:
            # Also, if it was fixed in or before LAST_HISTORIC_RELEASE.
            # Only things fixed in _recent_ releases belong on the front
            # page.
            return self.first_fixed_rel <= LAST_HISTORIC_RELEASE

def loadbugs(directory):
    ret = {}
    for bugname in os.listdir(directory):
        # .sw? filenames are the commonest Vim swap-file names on Unix
        if (bugname not in [".",".."]
                and not fnmatch.fnmatch(bugname, ".*.sw?")
                and not fnmatch.fnmatch(bugname, ".sw?")):
            try:
                filename = os.path.join(directory, bugname)
                ret[bugname] = Bug(bugname, filename=filename)
            except BugFormatError as e:
                raise BugFormatError(filename + ": " + str(e))
            except Exception as e:
                raise BugFormatError(filename + ": exception: " + str(e) +
                "\n" + traceback.format_exc())
    return ret

def link_bugs(bugs):
    for bug in bugs.values():
        bug.Blocks = []

    for bugname in sorted(bugs):
        bug = bugs[bugname]
        for i, dep in enumerate(bug.Depends):
            if dep not in bugs:
                raise BugFormatError(bugname + ": nonexistent dependency: " +
                                     dep)
            dep = bugs[dep]

            bug.Depends[i] = dep
            dep.Blocks.append(bug)

def generate_modification_dates(directory, bugs):
    gitlog = gitrepo.GitRepo(directory).git(
        ["log", "--reverse", "--pretty=format:%ci", "--name-only", "--", "."],
        postproc=None)
    for record in gitlog.split("\n\n"):
        lines = record.splitlines()
        timestamp = lines.pop(0)
        for line in lines:
            # Check the name of the modified file is inside the data subdir.
            prefix = "data/"
            if not line.startswith(prefix):
                continue
            bugname = line[len(prefix):]

            # Check the bug record still exists. (Might be defunct.)
            if bugname not in bugs:
                continue

            bugs[bugname].last_modified = timestamp

def expand_fixed_in(bugs, refs, is_ancestor, release_vers):
    # Start by identifying the git commit for each version we care
    # about. We use the special key Version.nextrel() to mean current
    # main, i.e. the development snapshots.
    gitids = {Version.nextrel(): refs["refs/heads/main"]}
    for rel in release_vers:
        if release_vers[rel]: # check if it's a pre-release or a release
            gitids[rel] = refs["refs/heads/pre-" + str(rel)]
        else:
            gitids[rel] = refs["refs/tags/"+str(rel)]

    for b in bugs.values():
        first_rel = None

        b.affected_releases = set()

        b.first_fixed_rel = None
        b.last_predating_rel = Version.dawnoftime()

        # Determine as rigorously as possible, via git commit
        # ancestry, whether this bug affects each release we know
        # about, including the special release 'None' == main.
        for rel, relgitid in gitids.items():
            fixed = predates = False

            for ver in b.Fixed_In:
                if ver.vertype() == "release":
                    vergitid = gitids[ver]
                elif ver.vertype() == "gitcommit":
                    vergitid = ver.githash()
                else:
                    continue # can't do anything with this type
                if is_ancestor(vergitid, relgitid):
                    fixed = True

            for ver in b.Absent_In:
                if ver.vertype() == "release":
                    vergitid = gitids[ver]
                elif ver.vertype() == "gitcommit":
                    vergitid = ver.githash()
                else:
                    continue # can't do anything with this type
                if is_ancestor(relgitid, vergitid):
                    predates = True

            if not (fixed or predates):
                b.affected_releases.add(rel)

            # Also, identify the first release that postdates all
            # affected versions; below, we'll add it to the Fixed-In
            # header if it's not already there.
            #
            # We do this based on the value of the 'fixed' flag alone,
            # i.e. not considering the Absent-In header, because that
            # way we can distinguish 'first release after fix' from
            # 'last release before bug was introduced' even if they're
            # consecutive releases (i.e. this bug was broken and fixed
            # between releases n and n+1).
            if fixed:
                b.first_fixed_rel = (rel if b.first_fixed_rel is None
                                     else min(b.first_fixed_rel, rel))

            # Similarly, identify the last release that predates the
            # introduction of the bug.
            if predates:
                b.last_predating_rel = max(b.last_predating_rel, rel)

        # Add first_fixed_rel to the Fixed-In header, if it's defined
        # to something meaningful and not already listed.
        if (b.first_fixed_rel is not None and
            b.first_fixed_rel != Version.nextrel() and
            b.first_fixed_rel not in b.Fixed_In):
            b.Fixed_In.append(b.first_fixed_rel.inferred_copy())

def format_bug_list(bugs, prioritise=lambda x:False, highlight_vulns=True):
    ret = ""
    ret += "<ul>\n"

    boldbugs = [bug for bug in bugs if prioritise(bug)]
    nonboldbugs = [bug for bug in bugs if bug not in boldbugs]

    for bold, bugs in [(True, boldbugs), (False, nonboldbugs)]:
        for bug in sorted(bugs, key=lambda b:b.bugname):
            bughtml = '<a href="%s.html">%s: %s</a>' % (
                html.escape(bug.bugname), html.escape(bug.bugname),
                html.escape(bug.Summary))
            if bug.Class == "vulnerability" and highlight_vulns:
                bughtml = '<b><i>%s</i></b>' % bughtml
            elif bold:
                bughtml = '<b>%s</b>' % bughtml
            ret += '<li>%s\n' % bughtml

    ret += "</ul>\n"
    return ret

def put_in_fixed_bucket(b, fixed_buckets, release_vers):
    # Check if this bug never actually appeared in a release, i.e.
    # it was both broken and fixed between a pair of successive
    # releases, or has been both broken and fixed since the most
    # recent release.
    released = any(b.last_predating_rel < v < b.first_fixed_rel
                   for v in release_vers)

    # Add this bug to an appropriate 'fixed' bucket.
    key = b.first_fixed_rel, released
    fixed_buckets.setdefault(key, [])
    fixed_buckets[key].append(b)

def format_fixed_issues(fixed_buckets, release_vers):
    ret = ""

    for (fixed_rel, released), bugs in sorted(
            fixed_buckets.items(), reverse=True):
        fixed_rel_str = fixed_rel.text_description(release_vers)
        if released:
            heading = "Fixed in %s" % fixed_rel_str
            fixed_anchor_frag = fixed_rel.anchor_fragment(release_vers)
        else:
            absent_rel = max(v for v in release_vers if v < fixed_rel)
            heading = "Broken and fixed between %s and %s" % (
                absent_rel.text_description(release_vers), fixed_rel_str)
            fixed_anchor_frag = None

        heading = html.escape(heading)
        if fixed_anchor_frag:
            heading = '<a name="fixed-%s">%s</a>' % (
                fixed_anchor_frag, heading)
        ret += "<h3>%s</h3>\n" % heading

        buglists = []
        if released:
            # For an ordinary release, we further split up the bugs by
            # class.
            ret += "<ul>\n"
            classes = {}
            ordering = {
                "vulnerability":0,
                "bug":1, "semi-bug":1, # lump these together
                "wish":2
            }
            extra = {
                0:("Security vulnerabilities","vuln"),
                1:("Bugs (and semi-bugs)","bug"),
                2:("Wishes","wish"),
            }
            for bug in bugs:
                classes.setdefault(ordering[bug.Class], [])
                classes[ordering[bug.Class]].append(bug)
            for orderval in sorted(classes):
                buglists.append(extra[orderval] + (classes[orderval],))
        else:
            # For the 'broken and fixed between' section, we only have
            # one list of bugs (on the assumption that we only
            # bothered to record the vitally important ones anyway).
            buglists.append((None, None, bugs))

        for subheading, type_anchor_frag, bugs in buglists:
            if subheading is not None:
                ret += '<li><a name="fixed-%s-%s">%s</a>\n' % (
                    type_anchor_frag, fixed_anchor_frag,
                    html.escape(subheading))
            if not released:
                prioritise = lambda bug:False
            else:
                prioritise = lambda bug:(
                     bug.Priority == 'high'
                    or bug.Difficulty not in (None, 'fun'))
            ret += format_bug_list(bugs, prioritise=prioritise,
                                   highlight_vulns=released)

        if released:
            # Close the extra <ul> we opened above.
            ret += "</ul>\n"

    return ret

def gen_index(bugs, baseurl, release_vers):
    # Loop over all the input bugs and categorise them.
    fixed_buckets = {} # key is (fixed_rel, released)
    unfixedvulns = []
    actualbugs = []
    semibugs = []
    wish_high = []
    wish_med = {diff:[] for diff in bug_difficulties}
    wish_low = {diff:[] for diff in bug_difficulties}
    wish_never = []
    unclassified = []
    for b in bugs.values():
        if b.obsolete:
            continue
        elif b.first_fixed_rel is None:
            # An unfixed item, i.e. still in the pending section.
            bucket = (
                unfixedvulns if b.Class == "vulnerability" else
                actualbugs if b.Class == "bug" else
                semibugs if b.Class == "semi-bug" else
                wish_high if (b.Class,b.Priority) == ("wish","high") else
                wish_med[b.Difficulty] if (b.Difficulty is not None and
                    (b.Class,b.Priority) == ("wish","medium")) else
                wish_low[b.Difficulty] if (b.Difficulty is not None and
                    (b.Class,b.Priority) == ("wish","low")) else
                wish_never if (b.Class,b.Priority) == ("wish","never") else
                unclassified)
            bucket.append(b)
        else:
            put_in_fixed_bucket(b, fixed_buckets, release_vers)

    ret = ""
    ret += textwrap.dedent("""\
    <html><head>
    <title>PuTTY standalone wishlist</title>
    </head><body>
    <h1>PuTTY standalone wishlist</h1>
    <p>The PuTTY website trappings will be wrapped around this by the main
    Mason site generator.</p>
    <a name="endheader"></a>
    """)

    ret += textwrap.dedent("""\
    <h2><a name="fixed">Recently fixed</a></h2>

    <p>
    These are items we believe we have addressed in a recent release.
    Not every change is documented here; see the <a
    href="%s/changes.html">Changes page</a> for further information
    about what was in each release.

    """) % (html.escape(baseurl),)

    ret += format_fixed_issues(fixed_buckets, release_vers)

    ret += textwrap.dedent("""\
    <h2><a name="fixed">Fixed in older releases</a></h2>

    <p>See the <a href="historic.html">historic wishlist page</a> for
    information about issues fixed in older releases.

    """)

    ret += textwrap.dedent("""\
    <h2><a name="pending">Outstanding issues</a></h2>

    <p>
    These are bugs still to be fixed and features remaining to be
    implemented.

    """)

    if len(unfixedvulns) > 0:
        ret += textwrap.dedent("""\
        <h3><a name="vulns">Vulnerabilities</a></h3>

        <p>
        These are potential or actual vulnerabilities in the code that may
        allow an attacker to gain some level of unauthorised access, or
        otherwise cause mischief.
        """) + format_bug_list(unfixedvulns)

    if len(actualbugs) > 0:
        ret += textwrap.dedent("""\
        <h3><a name="bugs">Bugs</a></h3>

        <p>
        These items are clearly actual problems and we want them fixed.
        """) + format_bug_list(actualbugs, lambda b: b.Priority=="high")

    if len(semibugs) > 0:
        ret += textwrap.dedent("""\
        <h3>Semi-bugs</h3>

        <p>
        These are things which might be bugs or might not, depending on your
        precise definition of what a bug is.

        """) + format_bug_list(semibugs, lambda b: b.Priority=="high")

    if any(len(x)>0 for x in itertools.chain(
           [wish_high, wish_never], wish_med.values(), wish_low.values())):
        ret += textwrap.dedent("""\
        <h3>Wishlist</h3>

        <p>
        These are things that have been requested by users or which
        seem to me like a good idea. Not all of these are likely to be
        implemented without outside help, and some of them will
        positively never be implemented.

        """)
        
        if len(wish_high) > 0:
            ret += textwrap.dedent("""\
            <p>
            Features we'd like to include in the next release:

            """) + format_bug_list(wish_high)

        if any(len(x)>0 for x in wish_med.values()):
            ret += textwrap.dedent("""\
            <p>
            Plausible features we hope to get round to adding at some point:

            <ul>
            """)
            for diff in bug_difficulties:
                if len(wish_med[diff]) == 0:
                    continue
                ret += '<li>"%s": %s\n%s' % (
                    html.escape(diff), html.escape(bug_difficulties[diff]),
                    format_bug_list(wish_med[diff])
                )
            ret += "</ul>\n"

        if any(len(x)>0 for x in wish_low.values()):
            ret += textwrap.dedent("""\
            <p>
            Features we're not sure about, or which probably won't get added
            unless someone else does the hard work:

            <ul>
            """)
            for diff in bug_difficulties:
                if len(wish_low[diff]) == 0:
                    continue
                ret += '<li>"%s": %s\n%s' % (
                    html.escape(diff), html.escape(bug_difficulties[diff]),
                    format_bug_list(wish_low[diff])
                )
            ret += "</ul>\n"
        
        if len(wish_never) > 0:
            ret += textwrap.dedent("""\
            <h4>Non-wish list</h4>

            <p>
            These are features we are actively opposed to seeing in PuTTY. Don't
            bother writing them and sending them to us! If you think you have a
            good argument why they would be good, feel free to plead for them, but
            generally I'll already have a good reason not to do them.

            """) + format_bug_list(wish_never)

    if len(unclassified) > 0:
        ret += textwrap.dedent("""\
        <h2>Unclassified</h2>

        <p>
        Usually items we don't know enough about, or haven't yet thought about
        enough, to fully classify.

        """) + format_bug_list(unclassified)

    ret += textwrap.dedent("""\
    <h2><a name="historic">Historic issues</a></h2>

    <p>
    Here's a link to a subpage of <a href="historic.html">historic
    issues</a>, which probably aren't interesting any more unless
    you're doing archaeology.

    """)

    ret += "</body></html>\n"
    return ret

def gen_historic_index(bugs, baseurl, release_vers):
    # Loop over all the input bugs and categorise them.
    fixed_buckets = {} # key is (fixed_rel, released)
    actualbugs = []
    semibugs = []
    wishes = []
    unclassified = []

    for b in bugs.values():
        if not b.obsolete:
            continue
        elif b.first_fixed_rel is None:
            # Build-time check to make sure I don't do this, or at least
            # if I do (and I can't currently think of a good reason at
            # all) then I think very hard about why.
            assert b.Class != "vulnerability", (
                "Don't make unfixed vulns historic!")

            # 'Dormant' applies to wishes, and 'historic' to bugs.
            if b.Class == "wish":
                assert b.Priority != "historic"
            else:
                assert b.Priority != "dormant"

            bucket = (
                unfixedvulns if b.Class == "vulnerability" else
                actualbugs if b.Class == "bug" else
                semibugs if b.Class == "semi-bug" else
                wishes if b.Class == "wish" else
                unclassified)
            bucket.append(b)
        else:
            put_in_fixed_bucket(b, fixed_buckets, release_vers)

    ret = ""
    ret += textwrap.dedent("""\
    <html><head>
    <title>PuTTY standalone historic wishlist</title>
    </head><body>
    <h1>PuTTY standalone historic wishlist</h1>
    <p>The PuTTY website trappings will be wrapped around this by the main
    Mason site generator.</p>
    <a name="endheader"></a>
    """)

    ret += textwrap.dedent("""\
    <h2><a name="fixed">Fixed in older releases</a></h2>

    <p>
    These are items we believe we addressed in a release too old to
    appear on the main wishlist page. See also the <a
    href="%s/changes.html">Changes page</a>.

    """) % (html.escape(baseurl),)

    ret += format_fixed_issues(fixed_buckets, release_vers)

    ret += textwrap.dedent("""\
    <h2><a name="dormant">Historic issues</a></h2>

    <p>
    These wishlist items are marked with priority 'historic' or
    'dormant', meaning that we think they're probably not relevant to
    current versions of PuTTY and current sensible uses of PuTTY, for
    some reason such as:
    <ul>
    <li>only occurred in circumstances that don't seem important any more
      <ul>
      <li>when running on a very old version of Windows
      <li>when talking to a very old server version
      <li>when speaking an obsolete protocol (like Rlogin)
      </ul>
    <li>hasn't been re-reported in ages, so might have gone away by
    itself without anyone noticing
      <ul>
      <li>fixed in passing by other work we did on PuTTY
      <li>fixed or made invisible by an OS update
      </ul>
    <li>so vague we can't work out what they were referring to any more
    <li>was a feature request for a use case that we don't think is
    interesting any more
    </ul>

    """)

    if len(actualbugs) > 0:
        ret += textwrap.dedent("""\
        <h3><a name="bugs">Historic bugs</a></h3>

        <p>
        These items were actual problems at one time; we doubt that
        they still are any more, or if they are, that anyone cares.
        """) + format_bug_list(actualbugs)

    if len(semibugs) > 0:
        ret += textwrap.dedent("""\
        <h3>Historic semi-bugs</h3>

        <p>
        These are things which might be bugs or might not, depending
        on your precise definition of what a bug is. Again we doubt
        these ones are still actual problems at all.

        """) + format_bug_list(semibugs)

    if len(wishes) > 0:
        ret += textwrap.dedent("""\
        <h3>Dormant wishes</h3>

        <p>
        These are things that have been requested by users or which
        seemed to us like a good idea at some point in the past. If
        they're declared dormant then we probably won't do anything
        about them, even if their priority was previously set to high.
        """) + format_bug_list(wishes)

    if len(unclassified) > 0:
        ret += textwrap.dedent("""\
        <h2>Unclassified</h2>

        <p>
        We never got round to classifying these even into bug /
        semi-bug / wish, and now maybe we never will.
        """) + format_bug_list(unclassified)

    ret += "</body></html>\n"
    return ret

def gen_vuln_list(bugs, rel, description, vulnlist_link):
    ret = ""

    ret += textwrap.dedent("""\
    <html><head>
    <title>PuTTY vulnerability list for %s</title>
    </head><body>
    <h1>PuTTY vulnerability list for %s</h1>
    <p>The PuTTY website trappings will be wrapped around this by the main
    Mason site generator.</p>
    <ul>
    <a name="endheader"></a>
    """) % (
        html.escape(description),
        html.escape(description)
    )

    for _, b in sorted(bugs.items()):
        if b.Class != "vulnerability":
            continue
        if rel not in b.affected_releases:
            continue

        ret += '<li><a href="%s/%s.html">%s</a>' % (
            html.escape(vulnlist_link),
            html.escape(b.bugname),
            html.escape(b.bugname))
        if (b.first_fixed_rel is not None and
            b.first_fixed_rel != Version.nextrel()):
            ret += " (fixed in release %s)" % (
                html.escape(str(b.first_fixed_rel)))
        elif len(b.Fixed_In) != 0:
            ret += (" (not fixed in any release, "
                    "only in the development snapshots)")
        else:
            ret += " (not yet fixed at all)"
        ret += "\n"

    ret += textwrap.dedent("""\
    <a name="startfooter"></a>
    </ul>
    </body></html>
    """)

    return ret

def gen_cve_list(bugs):
    ret = ""

    ret += textwrap.dedent("""\
    <html><head>
    <title>PuTTY vulnerability list indexed by CVE number</title>
    <style type="text/css">
    table.cve-table { border-collapse: collapse; }
    table.cve-table th { border: 1px solid black; text-align: left; padding: 0.2em 0.5em; }
    table.cve-table td { border: 1px solid black; vertical-align: top; padding: 0.2em 0.5em; }
    </style>
    </head><body>
    <h1>PuTTY vulnerability list indexed by CVE number</h1>
    <p>The PuTTY website trappings will be wrapped around this by the main
    Mason site generator.</p>
    <a name="endheader"></a>
    """)

    all_cveids = collections.defaultdict(list)
    for _, b in sorted(bugs.items()):
        for cveid in b.cveids:
            all_cveids[cveid].append(b)

    def cve_sort_key(key_value):
        return tuple(s if s.rstrip(string.digits) != "" else int(s)
                     for s in key_value[0].split("-"))

    ret += textwrap.dedent("""\
    <table class="cve-table">
    <tr><th>CVE ID</th><th>PuTTY bug database entries referring to it</th></tr>
    """)
    for cveid, bugs in sorted(all_cveids.items(), key=cve_sort_key):
        ret += '<tr><td><a href="{}">{}</a></td><td>'.format(
            html.escape(cve_url(cveid)), html.escape(cveid))
        bugtext = []
        for b in bugs:
            bt = '<a href="{}.html">'.format(html.escape(b.bugname))
            bt += html.escape(b.bugname)
            bt += '</a>'
            footnotes = []
            if b.Class != "vulnerability":
                footnotes.append("not classed as a vulnerability")
            if b.first_fixed_rel is not None:
                footnotes.append("fixed in {}".format(
                    html.escape(str(b.first_fixed_rel))))
            elif len(b.Fixed_In) == 0:
                footnotes.append("not yet fixed")
            else:
                footnotes.append("fixed in snapshots")
            bt += html.escape(" ({})".format("; ".join(footnotes)))
            bugtext.append(bt)
        ret += '<br/>'.join(bugtext)
        ret += '</td></tr>\n'
    ret += textwrap.dedent("""\
    </table>
    """)

    ret += textwrap.dedent("""\
    <a name="startfooter"></a>
    </body></html>
    """)

    return ret

class CollisionDict(dict):
    class Collision(Exception):
        def __init__(self, key):
            self.key = key
        def __str__(self):
            return "CollisionDict.Collision(%s)" % repr(self.key)
    def __setitem__(self, key, value):
        if key in self:
            raise self.Collision(key)
        dict.__setitem__(self, key, value)

def main():
    default_bug_dir = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                   "../data"))
    parser = argparse.ArgumentParser(
        description="Make the HTML pages for the PuTTY wishlist.")
    parser.add_argument("datadir", nargs="?", default=default_bug_dir, help=
                        'Directory to read bug description files from. '+
                        'Default is ../data relative to this script.')
    parser.add_argument("-o", "--outputdir", nargs="?", help=
                        'Directory to write HTML output to.')
    parser.add_argument("-c", "--project-checkout", help=
                        'Directory containing a checkout of the main project.')
    parser.add_argument("-g", "--project-git-repo",
                        default="https://git.tartarus.org/simon/putty.git",
                        help=
                        'git URL for the repository of the main project.')
    parser.add_argument("--audit-link", nargs="?", default=
                        ('https://git.tartarus.org/?p=simon/putty-wishlist.git'
                         ';a=history;f=data/{BUGID};hb=refs/heads/main'),
                        help=
                        ('Format of an audit trail link. Include the string'
                         ' {BUGID} which will be substituted for the bug ID.'))
    parser.add_argument("--commit-link", default=
                        ('https://git.tartarus.org/?p=simon/putty.git;'
                         'a=commitdiff;h={COMMIT}'),
                        help=
                        ('Format of a link to a particular commit in a web-'
                         'browsable git viewer. Include the string {COMMIT} '
                         'which will be substituted for the commit hash.'))
    parser.add_argument("--vulnlist-link", nargs="?", default='../wishlist',
                        help=
                        ('Relative URI for vulnerability list pages to use'
                         ' when linking to per-bug pages. (Without the '
                         ' trailing slash.)'))
    parser.add_argument("--base-url", nargs="?", default='..',
                        help=
                        ('URL of the base PuTTY website that this set of'
                         ' pages will behave as if embedded in. (Without the'
                         ' trailing slash.)'))
    parser.add_argument("--no-fetch", action="store_true",
                        help=
                        ('Suppress "git fetch" from the project repository.'))
    args = parser.parse_args()

    try:
        # Read the bug data.
        bugs = loadbugs(args.datadir)

        # Make links between bugs. Included in the try...except
        # because it might point out dangling references in Depends
        # headers.
        link_bugs(bugs)
    except BugFormatError as e:
        sys.stderr.write(str(e) + "\n")
        sys.exit(1)

    # Tell all the bugs what their audit-trail URLs are.
    for bugname, bug in bugs.items():
        bug.audit_url = args.audit_link.replace("{BUGID}", bugname)

    # Retrieve the git log and figure out the last-modified date of
    # each bug.
    generate_modification_dates(args.datadir, bugs)

    # Fetch the main project repository and do a reachability analysis
    # so as to derive Fixed-In release versions from git commit ids.
    # This scan also finds the complete list of known release and
    # pre-release versions.
    release_vers = {}
    try:
        temppath = None
        if args.project_checkout is not None:
            project_repo = gitrepo.GitRepo(args.project_checkout)
        else:
            temppath = tempfile.mkdtemp(suffix='.git')
            project_repo = gitrepo.GitRepo(temppath)
            project_repo.git(["init", "--bare"])
            if args.no_fetch:
                sys.stderr.write("Can't use --no-fetch with a temp checkout\n")
                sys.exit(1)

        refprefix = "refs/putty-wishlist/"
        if not args.no_fetch:
            project_repo.git(
                ["fetch", "--prune", args.project_git_repo,
                 "+refs/heads/*:"+refprefix+"refs/heads/*",
                 "+refs/tags/*:"+refprefix+"refs/tags/*"])

        heads = []
        refnames = {}
        for line in project_repo.git(["for-each-ref", refprefix]):
            commitkind, ref = line.split('\t', 1)
            commit, kind = commitkind.split(' ', 1)
            assert ref.startswith(refprefix)
            ref = ref[len(refprefix):]
            refnames[ref] = commit
            heads.append(commit)
            try:
                for prefix, prerel in [
                    ("refs/tags/",False), ("refs/heads/pre-",True)]:
                    if ref.startswith(prefix):
                        v = Version(ref[len(prefix):])
                        if v.vertype() == "release":
                            release_vers[v] = prerel
                        break
            except BugFormatError:
                pass # ignore non-version-number-shaped tags or branches

        ancestor = gitreach.ancestor_tester(
            project_repo, heads, parent_rewriter=extraparents.parent_rewriter)
    finally:
        if temppath is not None:
            shutil.rmtree(temppath)

    # Add extra values to each bug's Fixed-In headers by inferring
    # that if the bug was fixed in a given svn revision or snapshot
    # date, it must also be fixed in all releases after that.
    expand_fixed_in(bugs, refnames, ancestor, release_vers)

    # Build our list of output files, each with a function that
    # generates its content. We do this even if given no actual output
    # directory, so that we can throw an error if two filenames clash.
    outfiles = CollisionDict()
    try:
        # Main index file.
        outfiles["index.html"] = (
            lambda: gen_index(bugs, args.base_url, release_vers))
        outfiles["historic.html"] = (
            lambda: gen_historic_index(bugs, args.base_url, release_vers))
        # Per-release vulnerability lists.
        for rel in release_vers:
            title = "%s %s" % (
                "pre-release" if release_vers[rel] else "release",
                str(rel))
            outfiles["vulnlist-%s.html" % str(rel)] = (
                lambda rel=rel, title=title: gen_vuln_list(
                    bugs, rel, title, args.vulnlist_link))
        outfiles["vulnlist-snapshots.html"] = (
            lambda rel=rel: gen_vuln_list(
                bugs, Version.nextrel(), "development snapshots",
                args.vulnlist_link))
        # Per-bug files.
        for bugname, bug in bugs.items():
            html = bug.to_html(args.commit_link, bugs)
            outfiles[bugname+".html"] = lambda html=html: html
        # Table of referenced CVEs, which we have to generate after
        # calling bug.to_html() on each bug, because that function
        # populates the cveids field of the bug as a side effect.
        outfiles["cvelist.html"] = lambda: gen_cve_list(bugs)
    except CollisionDict.Collision as e:
        sys.stderr.write("duplicate output file name '%s'\n" % e.key)
        sys.exit(1)

    # Generate output.
    if args.outputdir is not None:
        for name, generator in outfiles.items():
            with open(os.path.join(args.outputdir, name), "w") as f:
                f.write(generator())

if __name__ == "__main__":
    main()
