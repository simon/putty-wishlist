extraparents = {
    '389c62fed4498f1b8c8194a962c563526b80c3de': ['430edda8ea3dc8807424158ad99eedf9549e573c'], # the latter is a cherry-pick to 0.53b of the former
    '01e14508705aef4563027292c450faa00f0ed403': ['7ae0dbde80dca8462eb53dc89070e0a32b163bef'], # the latter is a cherry-pick to 0.53b of the former
    '8d5f7e293e8d7f50140ddd5ca411c8f74f0c5ebd': ['6f88743f23b2c6ea2ff8a3b63dcdbc7f3f9029b2'], # the latter is a cherry-pick to 0.53b of the former
    '7c95ea19c88fc7a547184ed84276fb3a6e2a5ba1': ['997c082c3b334d425db3df153015d0ce5ad344ad'], # the former is a trunk commit that is a superset of the latter
    'a1125a8052e442f5a5a6090449b03f851547d590': ['52bb59be17104ce5920053a9c348e9beed72938e'], # the latter is a cherry-pick to 0.53b of the pre-Unix parts of the former
    'a1125a8052e442f5a5a6090449b03f851547d590': ['e05f560c21342b9ee57ca3fa32994256f7d24e0c'], # the latter is an 0.53b commit only, unnecessary on trunk due to dupprintf having been introduced in the previous commit 7c95ea19c88fc7a547184ed84276fb3a6e2a5ba1
    'a1125a8052e442f5a5a6090449b03f851547d590': ['012f4af4ba944b224c13fd48aad3b4da74eb5545'], # the latter is an 0.53b commit only, unnecessary on trunk due to dupprintf having been introduced in the previous commit 7c95ea19c88fc7a547184ed84276fb3a6e2a5ba1
    'ad867466796dc0f54b1189beb06b0b2c509b4da7': ['722786aa6d450a71bf93df50424cbd5bda52079b'], # the latter is a cherry-pick to 0.53b of the former
    '421726993172ee902470bd4c63c740061aa956f3': ['2a88ce62b6f6c43c7a811bb4dd8f198ef58bb4b6'], # the former is a merge to trunk of the latter; the latter also has a116464f6ef6b627f453ad0ba58bc9e5480b840f in its ancestry, removing a recent large piece of development to make a stable release branch, which we deliberately ignore on the basis that we consider trunk to already 'contain' that removal plus an invisible subsequent reinstatement
    '49e9abb645c15e6f5d30e85242811d2a4c3ebbe4': ['ee6559a90f74ece4418d285bc8e0e906c968786d'], # the former is the trunk commit on which the remaining stuff from the 0.55 branch was merged back
    'fd070c31bf24637ee2681577e63540304303eca0': ['0acb18c036eeb6998ca39b8c00f8f60680fa16e4'], # the former is the trunk commit on which the version-number bump from the 0.56 branch was merged back; everything prior to that on 0.56 is merges from trunk
    '893d187b81991a7b259ede864b7d18ae60c59589': ['124ed66bb9d78911e5ae36ab47962b453a403074'], # the former is a merge to trunk of the latter
    '97154e55da393270f9838a4441fd01a0918f96bf': ['254ea454dff21569780f697158eb4e513dd18014'], # the latter's parent 70fe897640c4b953ee646d2dc7239a6f1b72173a is a cherry-pick to 0.57 of the former; the latter commit itself is the final one on the 0.57 branch, which is a cherry-pick of some post-SVN-migration stuff that was already on trunk in 1b94cc85c677b3e72140035d13f8e9354efcbf96, plus a line-ending conversion of README.txt which was _never_ replicated on trunk but we seem to be happy with the current line-ending status of that file so never mind
    'c968d9fe8ea9a0fabc02ff7a384550ff350e819d': ['dcb36c3be516300922d46247d5801ec6f4e2900f'], # the latter is a cherry-pick to 0.62 of the former, which is the version-number bump for 0.62 applied on trunk
}

def parent_rewriter(commit, parents):
    return parents + extraparents.get(commit, [])
