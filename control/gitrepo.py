import subprocess
import pipes

class GitFailure(Exception):
    pass

class GitRepo(object):
    def __init__(self, directory):
        self.directory = directory
        self.gitcmd = "git" # FIXME: may one day need to make this configurable

    def git(self, args, indata="", postproc=lambda s:s.splitlines()):
        cmd = [self.gitcmd] + args
        subproc = subprocess.Popen(cmd, cwd=self.directory,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        outdata, errdata = subproc.communicate(indata)
        exitcode = subproc.wait()
        outdata = outdata.decode()
        if exitcode == 0:
            return postproc(outdata) if postproc is not None else outdata
        else:
            raise GitFailure("'git %s' returned status %d\n"
                             "in directory: %s\n"
                             "full command: %s\n"
                             "standard error:\n%s\n" % (
                    args[0], exitcode, self.directory,
                    " ".join(map(pipes.quote,cmd)), errdata.rstrip("\n")))

    def read_commits(self, heads, recordfn):
        for line in self.git(["log", "--format=%H %P"] + heads):
            words = line.rstrip("\n ").split(" ")
            assert len(words) > 0
            recordfn(words[0], words[1:])
