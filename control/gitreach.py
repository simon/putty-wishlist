import gitrepo

class CommitGraph(object):
    def __init__(self):
        # Two dictionaries. Each one has a key which is a commit id,
        # and a value which is an array of further commit ids. Commit
        # ids are 40-digit SHA1 hashes, or one of the special values
        # below.
        self.parents = {}
        self.children = {}

        # Special commit id that is inserted as a pretend parent for
        # root commits, so that everything has a common parent and no
        # special cases are needed in the case of multiple roots.
        self.Source = object()

        # Similarly, a special commit id that is inserted as a pretend
        # child for commits with no other child. Makes a good starting
        # point for depth-first search.
        self.Sink = object()

        self.parents[self.Source] = []
        self.children[self.Source] = []
        self.parents[self.Sink] = []
        self.children[self.Sink] = []

        self.finalised = False

    def add_commit(self, commit, parents):
        assert not self.finalised

        if len(parents) == 0:
            parents = parents + [self.Source]
        self.parents[commit] = parents[:]
        self.children.setdefault(commit, [])
        for par in parents:
            self.children.setdefault(par, [])
            self.children[par].append(commit)

    def postorder(self):
        stack = [(self.Sink, 0)]
        done = {}
        while len(stack) > 0:
            commit, index = stack.pop()
            parents = self.parents[commit]
            try:
                nextindex = next(i for i in range(index,len(parents))
                                 if parents[i] not in done)
                stack.append((commit, nextindex+1))
                stack.append((parents[nextindex], 0))
            except StopIteration:
                assert commit not in done
                done[commit] = True
                assert all(done[p] for p in parents)
                yield commit

    def finalise(self):
        assert not self.finalised
        self.finalised = True

        # Insert the Sink pseudo-commit.
        sinkparents = self.parents[self.Sink]
        for commit, children in self.children.items():
            if commit != self.Sink and len(children) == 0:
                children.append(self.Sink)
                sinkparents.append(commit)

        # Initialise every commit's label to be empty.
        self.labels = {commit:[] for commit in self.parents}

        # Repeatedly find a chain taking in as many commits as
        # possible that haven't yet been visited, and add to each
        # commit's label an integer indicating the highest-ranked
        # element of that chain in its ancestry.
        done = {commit:False for commit in self.parents}
        while True:
            # Find our chain, by a dynamic-programming search which
            # stores in score[commit] a tuple consisting of the number
            # of new commits in the best chain ending in that commit,
            # and the previous commit in that chain (if any).
            score = {}
            for commit in self.postorder():
                this_score = 1 if not done[commit] else 0
                best_score, best_parent = this_score, None
                for par in self.parents[commit]:
                    par_score = this_score + score[par][0]
                    if par_score >= best_score:
                        best_score, best_parent = par_score, par
                score[commit] = (best_score, best_parent)

            # Dig the actual chain out of score[], and terminate this
            # loop if it hasn't managed to add _any_ new commits -
            # that's the end condition in which we have a full chain
            # cover of the dag.
            #
            # The nice thing about having a guaranteed sink commit is
            # that we know exactly where to look for the endpoint of
            # our best chain after that search. Otherwise we'd have to
            # keep track over all possible endpoint commits of the
            # best one we'd ended up with.
            if score[self.Sink][0] == 0:
                break
            chain = [self.Sink]
            while chain[-1] != self.Source:
                chain.append(score[chain[-1]][1])

            # Append a number to each commit's label based on this
            # chain.
            chain.reverse()
            ranks = {commit:i for i,commit in enumerate(chain)}
            for commit in self.postorder():
                if commit in ranks:
                    newlabel = ranks[commit]
                else:
                    newlabel = max(self.labels[par][-1]
                                   for par in self.parents[commit])
                self.labels[commit].append(newlabel)

            # Mark everything in this chain as done, and go round
            # again.
            for commit in chain:
                done[commit] = True

    def is_ancestor_of(self, putative_ancestor, putative_descendant):
        # Having labelled every commit with its highest ancestor in
        # each of a set of chains covering the whole graph, ancestry
        # testing is now easy, because A <= B in the graph if and only
        # if label[A] <= label[B] in the product order, i.e. B's
        # highest ancestor in every chain is at least A's.
        #
        # Proof:
        #
        # If A <= B, then any ancestor of A in a given chain is also
        # an ancestor of B, so B's highest ancestor can't be less than
        # A's. Hence, label[A][c] <= label[B][c] for all chains c.
        #
        # And if A,B are incomparable, then there has to be some chain
        # c containing A (because the chains are exhaustive), and it
        # doesn't contain B (because A,B can't be on the same chain);
        # so B's highest ancestor on that chain must be less than A
        # itself whereas A's highest ancestor on that chain _is_ A
        # itself. So we have label[A][c] > label[B][c] for that chain.
        # And conversely, there must also be another chain in which
        # the reverse applies, so we have label[A] and label[B]
        # incomparable in the product order. []
        return all(a <= d for (a,d) in zip(self.labels[putative_ancestor],
                                           self.labels[putative_descendant]))

def ancestor_tester(repo, heads, commit_list=None,
                    parent_rewriter=lambda c,p:p):
    graph = CommitGraph()

    def add_commit(commit, parents):
        if commit_list is not None:
            commit_list.append(commit)
        graph.add_commit(commit, parent_rewriter(commit, parents))

    repo.read_commits(heads,add_commit)
    graph.finalise()

    return graph.is_ancestor_of

def main():
    import sys
    import timeit

    repo = gitrepo.GitRepo(None)
    commits = []
    stuff = []
    def make_tester():
        stuff.append(ancestor_tester(repo, ["main"], commits))
    duration = timeit.timeit(make_tester, number=1)
    print("Built graph in %.2f seconds" % duration)
    ancestor_of = stuff[0]

    print("Testing mutual ancestry between %d commits" % len(commits))
    ntests = 0

    for nouter, c1 in enumerate(commits):
        if sys.stdout.isatty():
            sys.stdout.write(" %d / %d\r" % (nouter, len(commits)))
            sys.stdout.flush()
        c1_ancestors = repo.git(["rev-list", c1])
        for c2 in commits:
            ntests += 1
            our_ancestor = ancestor_of(c2, c1)
            git_ancestor = c2 in c1_ancestors
            assert our_ancestor == git_ancestor, (
                "Failed test ancestor_of(%s,%s): we say %s, git says %s") % (
                repr(c1), repr(c2), repr(our_ancestor), repr(git_ancestor))

    assert ntests == len(commits)**2, (
        "Wrong number of tests! %d should be %d^2 but is not") % (
        ntests, len(commits))
    print("All %d tests passed" % ntests)

if __name__ == "__main__":
    main()
