Summary: PuTTY's Windows Store entry has "unknown version" according to Intune and Winget 
Class: semi-bug
Priority: high
Difficulty: taxing
Present-in: 0.82
Content-type: text/x-html-body

<p>We've had a couple of reports that when automated installer tools
try to install PuTTY from its entry in the Windows Store, they aren't
able to determine its version number.

<p>One of the tools in question is "Intune",
presumably <a href="https://en.wikipedia.org/wiki/Microsoft_Intune">this
one</a>. Intune reports a GUI error message reading "The selected app
does not have a valid latest package version."

<p>We've had a similar problem reported from "winget",
presumably <a href="https://github.com/microsoft/winget-cli">this
one</a>. A CLI command like "<code>winget search putty</code>" finds
our real Store entry (with id <code>XPFNZKSKLBP7RJ</code>), but
reports a version field of "Unknown", in a display looking something
like this (probably including other search hits too):

<pre>
Name    Id              Version      Match     Source
------------------------------------------------------
PuTTY   XPFNZKSKLBP7RJ  Unknown                msstore
</pre>

<p>We'd like to fix this, but we don't know how. When we fill in the
web form that submits a new version of PuTTY into the Store, we
haven't found any field of the form where you enter a version number.
So if the Store knows the version numbers of some packages but not
PuTTY, we don't know what we're supposed to do to fix that.

<p>Therefore, I've classified this as "semi-bug", because it's not
clear whether there's anything <em>we</em> can do to fix it (the
problem might also be in the Store itself, or in the tools that access
it), and also as "taxing" because it's not just a question of us doing
work &#8211; we also need information we don't have.

<p>SGT's best guess, based on skimming the source code of "winget" on
Github, is that it might have something to do with PuTTY's Store entry
being based on the MSI installer system, instead of MSIX (which is
more standard for Store entries). In "winget" there's a piece of code
that extracts a version number from a metadata field in a MSIX file,
but no corresponding code for MSI. And MSI integration with the Store
is newer than MSIX. So one possibility (as far as we know) is that
version number detection just doesn't work for MSI apps.

<p>But if anybody knows better, or can tell us what we have to do to
make PuTTY's Store entry have useful version metadata, we'd like to
hear from you!
