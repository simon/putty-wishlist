Summary: Vulnerability: DSA signature check bypass (development code only)
Class: vulnerability
Difficulty: fun
Priority: high
Absent-in: 0.70 2018-12-31
Present-in: 25b034ee39f557cab6e6e7b79591ef46c72cba92 2019-01-01
Fixed-in: 8957e613bc1c2574a6ab1190244ae2b4868831ed 2019-02-11
Content-type: text/x-html-body

<p>
Some development snapshot versions of PuTTY have a vulnerability
allowing a man-in-the-middle attacker to compromise (view and modify)
SSH sessions, silently in some circumstances. <b>No release version of
PuTTY is affected by this bug, including 0.70.</b> Only development
snapshot builds from us dated 2019, before 2019-02-11, are affected.

<p>
The bug affects DSA signature checking; in vulnerable versions, there
is a fixed signature that an attacker can present which will always
pass a signature check regardless of anything else. (See the Fixed-in
commit message for the precise details.) Other signature algorithms
(including ECDSA and Ed25519) are not affected.

<p>
The main impact of this is on the use of DSA ("ssh-dss") format host
keys. The precise effect of this depends on the existing contents of
the client's host key cache.
<ul>
<li>If PuTTY has any cached ssh-dss key for the server that the client
is trying to connect to, the man-in-the-middle attacker can silently
compromise the connection (even if PuTTY and the server would not
normally choose to use the DSA host key; the attacker can force a
downgrade, undetected).</li>
<li>If PuTTY has no DSA host key for the target server, and the server
has a real DSA host key, the attacker can arrange that the host key
confirmation dialog presented to the client user is the correct one
(so that if the user checks the host key out of band) and still
compromise the connection if that key is accepted. In this case, the
user has an opportunity to smell a rat, as use of DSA host keys is
quite unusual, or a change of host key algorithm could be
unexpected.</li>
</ul>

<p>
If your PuTTY installation has no DSA host keys cached (on Windows you
can check this by inspecting the Registry), and you're sure you
haven't been prompted for one while using a vulnerable snapshot of
PuTTY, then you are probably fine. (To reiterate: if you have only
used released versions of PuTTY, then you are definitely fine.)

<p>
If the client (or user) insists on public-key <em>user</em>
authentication (even with a DSA user key), this vulnerability is
somewhat mitigated; the man-in-the-middle cannot gain access to the
server (as they don't have access to the user's private key), although
they can still pretend to be the server to the user using any prior
knowledge of the server they may have.

<p>
This vulnerability was found by Filipe Casal, as part of a
<a href="https://hackerone.com/putty_h1c">bug bounty programme</a> run under
the auspices of the EU-FOSSA project. Since it only affected
pre-release code, we disclosed the fix immediately.
