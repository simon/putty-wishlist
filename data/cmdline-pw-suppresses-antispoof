Summary: Use of -pwfile (and -pw) options suppresses Plink anti-spoofing defences
Class: bug
Absent-in: 88d5bb2a222470f1f615d96384f3701f1406790d
Present-in: a82ab70b0bf418a7c18d07e0090bbf194f795cbe 0.77
Fixed-in: bdb3ac9f3b80b0c94c05261abc9cbe7b89a9ebea
Content-type: text/x-html-body

<p>
Since 0.77 (specifically the fix for
<bug id="cmdline-pw-fallback-to-interactive"/>), using Plink's
<code>-pwfile</code> option (or the deprecated <code>-pw</code>
option) to supply a password would accidentally suppress Plink's
<bug id="vuln-auth-prompt-spoofing">anti-spoofing defence</bug>; the
usual 'Access granted. Press Return to begin session' prompt that you
get in an interactive session would not appear. (This is
Plink-specific; other tools, such as PuTTY and PSFTP, are not
affected.)

<p>
This is now fixed.

<p>
(It remains the case that if this prompt is inconvenient for you, and
you aren't worried about the mischief it protects against, you can
<a href="https://tartarus.org/~simon/putty-snapshots/htmldoc/Chapter7.html#plink-option-antispoof">turn it off</a>.)
