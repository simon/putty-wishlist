Summary: Interactive username/password prompts for proxy authentication
Class: wish
Priority: medium
Difficulty: tricky
Fixed-in: c1ddacf78f75c463035a03371d2477d30ffa92ba
Content-type: text/x-html-body

<p>
Currently, proxy passwords have to be stored unencrypted in the
registry, or entered manually through the setup dialogue before every
session. It should be possible to enter them at the time the
connection is set up, like other passwords. This would also make use
of authentication schemes where the password is always changing more
practical.

<p>
2021-12: this is now implemented, using the same refactoring that
enabled <bug id="ssh-proxy"/> to provide <em>its</em> SSH login
prompts. For all proxy types, if a username or password is required by
the proxy but not provided in the session configuration, PuTTY will
interactively prompt for it in the terminal window.

<p>
Limitation: interactive proxy login can only be used in context where
an interactive prompt is feasible at all. In particular, if you use
SSH reverse port forwarding to direct a forwarded connection out
through a proxy once the SSH session has started, <em>that</em> proxy
connection will not be able to prompt interactively for a password
(the terminal window is already in use), and will fall back to failing
to authenticate.
