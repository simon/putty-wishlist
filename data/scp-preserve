Summary: PSCP in SCP mode messes up file download with '-p' option
Class: bug
Priority: high
Absent-in: 0.70
Present-in: 0.71
Fixed-in: 4135e5d29550654227e275907e63a58e232cce38
Content-type: text/x-html-body

<p>
PSCP, when it is using the SCP protocol (which is rare), and has been
configured to preserve timestamps and permissions (with <tt>-p</tt>)
on a downloaded file can, due to a mistake in the protocol
implementation, get the file name wrong, create a local file with an
unhelpful mode, get the size wrong and perhaps hang, maybe print bits
of the file content to the console, and generally makes a complete
hash of things. For instance:

<pre>$ ./pscp -scp -remotehost:stoat .
Keyboard-interactive authentication prompts from server:
| Password:
End of keyboard-interactive prompts from server
warning: remote host tried to write  to a file called '0 1559391040 0
0664 160166 stoat
'
         when we requested a file called 'stoat'.
         If this is a wildcard, consider upgrading to SSH-2 or using
         the '-unsafe' option. Renaming of this file has been disallowed.
stoat                     | 32 kB |  32.0 kB/s | ETA: 00:04:45 |   0%
[hang]</pre>

<p>
In most circumstances, PSCP will use the more modern SFTP protocol,
which does not have this problem. The SCP protocol will only be used
if the server doesn't appear to support SFTP (rare), or if the user
explicitly specified the <tt>-scp</tt> option on the command line.
