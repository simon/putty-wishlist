Summary: Use PuTTY as a local GUI terminal app for the Cygwin shell
Class: wish
Priority: low
Difficulty: taxing
Fixed-in: 0.76
Content-type: text/x-html-body

<p>
It would be nice to be able to use PuTTY as a local terminal window
application for Cygwin shell sessions: a sort of local
<code>xterm</code>-alike (or rather, <code>pterm</code>-alike).

<p>
There are programs derived from the PuTTY code which do this directly
(in particular <code>mintty</code>), so one option is to use those
instead. However, those also make other changes from PuTTY's defaults,
which you might not like.

<p>
If you prefer to use the original upstream PuTTY, the way to achieve
this is to have two processes: PuTTY to provide the GUI terminal, and
a subprocess linked against Cygwin to talk to the pty.

<p>
As of PuTTY 0.76, the recommended way to do this is using the 'Bare
ssh-connection' protocol supported by PuTTY, with the subprocess being
a Cygwin-compiled version of the <code>psusan</code> tool in the Unix
PuTTY distribution which acts as a server for that protocol. This not
only allows you to run a Cygwin terminal session, but also to forward
Pageant to the Cygwin environment so that the Cygwin SSH client can
also find it.

<p>
Since <code>psusan</code> is fully productised as part of the 0.76
Unix PuTTY tool set, we're declaring this wishlist item to be as done
as it's going to be. The only significant improvement possible in
principle would be to make PuTTY itself do the whole job of talking to
Cygwin pty devices &#8211; but in order to do that it would have to be
linked against the Cygwin standard library, and that's too specialist
a thing to make the standard <code>putty.exe</code> do.

<p>
Our <a href="../links.html">Links page</a> links to a modified version
of PuTTY called PuTTYcyg, which was able to act as a Cygwin local
terminal (again using a helper process linked against Cygwin) before
PuTTY proper could do so.
