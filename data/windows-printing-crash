Summary: Printing on Windows causes a crash
Class: bug
Difficulty: fun
Priority: high
Absent-in: 73039b7831aa863fabba1e6ff06471643303ae09
Present-in: 793ac872757667a87df4636b5b3eed61302dd837
Fixed-in: 892d4a0188ffd8aa60dc11b4bace30dfb0f9d50e
Content-type: text/x-html-body

<p>
PuTTY 0.69 segfaulted when attempting to print.

<p>
This was because, as part of the fix for <bug
id="vuln-indirect-dll-hijack-2"/>, we switched to accessing the
Windows printing API by loading DLLs dynamically at run time rather
than at load time (i.e. after we lock down the default DLL search path
to defend against hijacking attacks).

<p>
However, our mistake was to believe the MSDN API when it said that
some of the printing API functions we use should be found in
<code>SPOOLSS.DLL</code> and others in <code>WINSPOOL.DRV</code>. In
fact, <code>SPOOLSS.DLL</code> does not exist on all versions of
Windows, so we were failing to load it or any of the functions we
tried to find in it.

<p>
If you use those functions in the normal (load-time) way, Visual
Studio's libraries generate a DLL import table that expects to find
all of them in <code>WINSPOOL.DRV</code>. We think that this is
therefore the <i>de facto</i> right place to look for them, no matter
what the MSDN pages for the individual functions might think, and
that's what we've switched to doing for 0.70.
