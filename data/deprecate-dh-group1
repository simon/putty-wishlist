Summary: Deprecate key exchange method diffie-hellman-group1-sha1
Class: semi-bug
Difficulty: fun
Priority: high
Fixed-in: 34add87ad249205d4ed36381bfb506a431dc0e7a 2016-04-11
Content-Type: text/x-html-body

<p>
<a href="https://weakdh.org/">weakdh.org</a> points out that 1024-bit
Diffie-Hellman groups are susceptible to precomputation by a
well-resourced attacker.

<p>
The fixed 1024-bit Oakley Group 2 used in the
diffie-hellman-group1-sha1 SSH key exchange method is also used by
other protocols, so looks like an attractive target.

<p>
By default, PuTTY now warns if the diffie-hellman-group1-sha1 key
exchange method is negotiated. Existing saved sessions which match
PuTTY's old defaults will be changed accordingly (except for a corner
case where a session was saved with an unreleased development snapshot
between the points where we added ECDH and made this change, a period
of about 18 months). Non-default settings will be left alone, on the
assumption that the user knows what they're doing.

<p>
The other fixed group, the 2048-bit one used in
diffie-hellman-group14-sha1, is still allowed (although you can
configure it not to be). PuTTY's default is to prefer ECDH
(Elliptic-Curve Diffie-Hellman) or DH group exchange above any fixed
groups, if the server claims to support them.

<p>
If the server chooses Oakley Group 2 during group exchange (as the
weakdh.org paper claims is quite common), PuTTY does not complain.
(This exchange is protected by the host key, so an active attacker
shouldn't be able to substitute the prime of their choice.)
