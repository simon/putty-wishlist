Summary: PSCP should strip unprintable characters from SCP server output
Class: bug
Difficulty: tricky
Priority: high
Present-in: 0.70
Fixed-in: 2675f9578daf662ea5338e54ebe4825b2ff35597
Content-type: text/x-html-body

<p>
When PSCP is downloading files from a server, it writes the names of
the copied files to its standard output as part of progress reports.
Also, if the SCP server writes data to its standard <em>error</em>
channel, then PSCP will pass that through to its own standard error.

<p>
In neither case does PSCP take any care to sanitise the output. So if
a server really wants to, it can corrupt the display of the terminal
you run PSCP in, by sending escape sequences or other control
characters such as backspace.

<p>
This isn't really part of the implied contract between an SCP client
and its user (unlike Plink, which wouldn't be doing its job if it
<em>didn't</em> pass through all the server's output unmodified). It
would be better to sanitise the output down to just printable
characters and newlines, and perhaps also to prefix server stderr data
with something that made it clearly look like a different kind of
thing from file download progress reports.

<p>
This bug was found and reported by Harry Sintonen as part of a piece
of security research. It has two CVE numbers:
<cve id="CVE-2019-6109"/> (relating to file names sent by the server)
and <cve id="CVE-2019-6110"/> (relating to the server's stderr
stream).

<p>
However, in spite of the existence of CVE numbers, we regard this as
only a bug, not a vulnerability. In the context of Harry Sintonen's
<a href="https://sintonen.fi/advisories/scp-client-multiple-vulnerabilities.txt">full advisory</a>,
the server's ability to send escape sequences is dangerous because it
can be used to hide the evidence of having exploited a more serious
vulnerability that lets an SCP server write to the client's filesystem
in ways the client did not intend to permit. But the research found
that PSCP does not have that more serious vulnerability (though other
SCP clients did). So in our case, the server wouldn't be able to
perform that attack in the first place, and would gain no benefit from
the ability to hide the evidence of it.
