Summary: Windows PuTTY as installed by the MSI can't find its help file
Class: bug
Absent-in: ffa25be185be5f27e8cebc3b7a5902a760671af6 0.77
Present-in: 6143a50ed228fdf5a72c2970629b4ff643d001fc 0.78
Fixed-in: 7339e00f4ac8a0e5f0e101d2a0effa18ef794034 c14f079863ba6fb48137c7a96be7de5f7e5a8007
Content-type: text/x-html-body

<p>When PuTTY 0.78 or 0.79 is installed on Windows using the MSI
installer package, the tools can't automatically find their help file:
for example, the 'Help' button in the GUI configuration box is missing.

<p>This was caused by a registry permissions mistake. The MSI installs
a registry entry to tell the tools where to find the installed copy of
the help file. That registry entry is installed systemwide in
<code>HKEY_LOCAL_MACHINE</code>, which means that normal users have
read-only access to it. But a refactoring of PuTTY's registry access
routines introduced a bug in which the tools would try to open that
key for read-write access (even though they only planned to read from
it afterwards), so the attempt would fail, and the tool wouldn't be
able to read the help file pathname.

<p>The help file <code>putty.chm</code> is present in the install
directory, and you can open it by hand if you look for it there. The
only problem is that the tools don't offer a convenient button to open
it.

<p>This also didn't affect the standalone version of
<code>putty.exe</code> that you can download from the website by
itself. That contains its own help file in a data resource, so it
doesn't depend on that registry entry anyway.
