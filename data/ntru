Summary: Support for NTRU Prime post-quantum key exchange
Class: wish
Fixed-in: faf1601a5549eda9298f72f7c0f68f39c8f97764
Content-type: text/x-html-body

<p><a href="https://www.openssh.com/releasenotes.html#8.9">OpenSSH 8.9</a>
and later have added support for the 'Streamlined NTRU
Prime' key exchange method, a lattice-based algorithm intended to
resist quantum attacks. PuTTY now supports it too.

<p>The system is run in parallel with Curve25519, which is the most
commonly used conventional key exchange method these days. So it's at
least no <em>less</em> secure than what you were probably using
already: to replicate the hashing operation that creates the session
keys, an attacker would have to figure out the shared secret output
from <em>both</em> methods. So even if NTRU were to turn out to have a
huge undiscovered flaw that made that easy, the attacker would still
need to break Curve25519 <em>as well</em>.

<p>Due to this safety precaution, we decided it was safe to enable
this by default and put it high on the KEX preference list. So if you
use an up-to-date version of PuTTY with existing configuration, it
will probably have already started speaking NTRU to compatible
servers, and (assuming all of this performs as intended) your sessions
will be safe even against an adversary recording the network traffic
now and intending to apply a quantum computer to it once one becomes
available.

<p>This method is called 'NTRU Prime / Curve25519 hybrid kex' in
PuTTY's user interface. For those who prefer to think in terms of raw
SSH protocol ids, the id for this combined system is
<code>sntrup761x25519-sha512@openssh.com</code>.
