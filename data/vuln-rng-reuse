Summary: Cryptographic random numbers can occasionally be reused
Class: vulnerability
Difficulty: fun
Priority: high
Fixed-in: 320bf8479ff5bcbad239db4f9f4aa63656b0675e 0.71
Content-Type: text/x-html-body

<p>
Up to and including version 0.70, PuTTY's cryptographic random number
generator could occasionally use the same batch of random bytes twice.

<p>
This occurred because of a one-byte buffer overflow in the random pool
code. If entropy from an external source was injected into the random
pool exactly when the current-position index was pointing at the very
end of the pool, it would overrun the pool buffer by one byte and
overwrite the low byte of the position index itself.

<p>
If the index was increased by this overwrite, then a range check would
fail, and everything would be reset to a sensible state. But if the
index was <em>decreased</em> (which is possible, since the pool size
of 1200 is not a multiple of 256), then previously output random
numbers could be accidentally recycled.

<p>
It's not clear to us whether this could be exploited on purpose.

<p>
As of 0.71, that entire random number generator has been completely
replaced with a freshly written one based on Schneier and Ferguson's
‘Fortuna’ design, so the affected code is completely absent.

<p>
If anybody needs a point fix for this issue in 0.70 without applying
the entire RNG rewrite, there is one available as a
<a href="https://salsa.debian.org/ssh-team/putty/commit/ac3bf240f22558a1a3ed5523fbfbb240c7637c74">commit</a>
in Debian's downstream packaging repository for PuTTY.

<p>
This vulnerability was found by Marius Wachtler, as part of a
<a href="https://hackerone.com/putty_h1c">bug bounty programme</a> run under
the auspices of the EU-FOSSA project.
It has been assigned CVE ID <cve id="CVE-2019-9898"/>.
