Summary: Automatic fallback from certified to uncertified SSH keys during userauth
Class: wish
Priority: low
Difficulty: fun
Content-type: text/x-html-body

<p>If you use a <bug id="ssh2-openssh-certkeys">public key with an
OpenSSH certificate</bug> as your user authentication key, it might be
useful to have an easy way to treat it as being two keys in one: the
version with the certificate, and the same key with the certificate
removed. (The same private key works to generate signatures for either
one.)

<p>For example, you might set up a single private key that you use to
log in everywhere; some of your destination hosts might accept it by
virtue of a CA having signed it, and others might accept the same key
because you directly entered it in their <code>authorized_keys</code>
or equivalent. (The latter kind of host might not even be running an
SSH server that speaks the certificate formats at all.) In that
situation, it might be convenient to configure all your client
connections the same way, so that PuTTY would offer the certified and
uncertified versions of the same key and let each server decide which
one it wants to accept.

<p>In the current state of the certificates implementation, the only
easy way to do that is to load both versions of the key into Pageant.

<p>It would be possible to add a special-purpose 'automatic fallback'
mode, in which any certified key that PuTTY received through any
channel at all would be automatically offered to the server in both
versions, whether it was in Pageant, or loaded from a certified PPK
file, or created at run time via the detached certificate config
option (so that the detached certificate was treated as optional).

<p>I don't currently (as of 2022-07-30) plan to do this proactively,
but I record the possibility here to see if anyone else thinks it
would be useful.

<p>(It would have to be a configurable option, because some servers
will disconnect after too many authentication attempts, and each
public key you offer counts as one. Also, some certificates are
semantic, e.g. come with a particular forced command baked in; in that
context, the certified and uncertified public keys aren't
interchangeable.)
