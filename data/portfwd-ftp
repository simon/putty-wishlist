Summary: Support for tunnelling FTP
Class: wish
Priority: dormant
Difficulty: tricky
Depends: mdpi
Content-type: text/x-html-body

<p>
FTP is inherently difficult to tunnel over SSH, because it uses
multiple network connections, so the `obvious' approach of just
forwarding port 21 doesn't work.

<p>
To support this, PuTTY would need to grovel inside inside the
application-layer protocol and rewrite PORT commands and such, or
alternatively, provide an FTP proxy.

<p>
Either of these would be quite a large and fiddly feature, and given
that we already support alternative secure file-transfer solutions
(scp, sftp), we're unlikely to implement this ourselves. In fact, in
order not to bloat the core of PuTTY, we probably wouldn't contemplate
it at all unless it could be implemented as a separate DLL.

<p>
As a special case, a SOCKSified FTP client can work with PuTTY's existing
dynamic forwarding <em>if</em> you use passive mode. (This requires
that both FTP client and server support it.)

<p><b>SGT, 2024-11-17</b>: classifying this wish as dormant. FTP is
fading into the past now that everyone prefers encrypted file transfer
protocols (either HTTPS or SFTP), so it doesn't seem vital any more to
add extra complexity in port forwarding to support it. Newer protocols
are aware that they might be running over NATs or proxies, and avoid
this kind of connection interdependence in the first place.
