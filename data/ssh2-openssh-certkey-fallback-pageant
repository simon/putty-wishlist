Summary: Pageant could automatically add the uncertified version when loading a certified SSH key
Class: wish
Priority: low
Difficulty: fun
Content-type: text/x-html-body

<p>If you load a <bug id="ssh2-openssh-certkeys">public key with an
OpenSSH certificate</bug> in Pageant, it's possible (in fact, easy)
for Pageant to reconstruct the uncertified version of the public key.
So it could be made to automatically do so, and present that as an
alternative when the next client tried to list the keys.

<p>This definitely shouldn't be done unconditionally, for the same
reasons as in <bug id="ssh2-openssh-certkey-fallback"/>. But it could
be made a configurable option in Pageant.

<p>I don't currently (as of 2022-08-06) plan to do this proactively,
but I record the possibility here to see if anyone else thinks it
would be useful.
