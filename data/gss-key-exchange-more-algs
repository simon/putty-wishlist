Summary: More GSSAPI key exchange algorithms (more groups/hashes, elliptic-curve)
Class: wish
Fixed-in: cec8c87626b3433907d214c91a072f75fbd06c91
Content-type: text/x-html-body

<p>GSSAPI key exchange works by using an existing SSH key exchange method
together with GSSAPI, and having GSSAPI authenticate the output.

<p>From <bug id="gss-key-exchange">PuTTY's initial implementation of GSS
key exchange</bug> up to and including 0.77, PuTTY implemented only the
originally standardised GSSAPI key exchange methods, all using integer
Diffie-Hellman and SHA-1.
But now we've added many more methods which were standardised later
(<rfc id="8732"/>):
<ul>
<li>Elliptic-curve Diffie-Hellman, using either the NIST curves or
Curve25519;</li>
<li>Integer Diffie-Hellman with the same new fixed groups and hashes
as just implemented in <bug id="rfc8268-dh-groups" />;</li>
<li>Integer Diffie-Hellman with the already-available 2048-bit
"group14" fixed group, but using SHA-256 as a hash function instead of
SHA-1.</li>
</ul>
<p>This brings the available set of GSSAPI-authenticated key exchange
methods much closer to parity with those used for ordinary key exchange.
<p>Neither SHA-1, nor small groups for integer Diffie-Hellman, will
now be used unless the server doesn't support anything better.
