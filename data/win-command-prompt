Summary: Ability to backend on to a Windows command interpreter
Class: wish
Difficulty: mayhem
Priority: low
Fixed-in: a55aac71e4fa6cf30f976da6727877c4cb117c96
Content-type: text/x-html-body

<p>
Lots of people used to ask whether PuTTY would be able to act as a
front end for Windows command interpreters (Command Prompt, bash,
etc), in place of the not-very-pleasant standard console window.

<p>
Some advantages of this (and probably not all of them):
<ul>
<li>You get the same free choice of fonts as PuTTY has: no restriction
to a strange subset of the system's available fonts.
<li>You get the same copy-paste gestures as PuTTY: no mental
gear-shifting when you have command prompts and SSH sessions open on
the same desktop.
<li>You get scrollback with the PuTTY semantics: scrolling to the
bottom gets you to <em>where the action is</em>, as opposed to the way
you could accidentally find yourself 500 lines past the end of the
action in a real console.
</ul>

<p>
This wishlist item was raised at Mayhem difficulty, because at the
time it was originally suggested, there was essentially no way to do
it reliably. I (SGT) did some experimental work, which involved
instantiating a Windows console as a hidden window, and using the same
console API used by applications running in the console to read out
its screen buffer and transfer that into the PuTTY window. This was
unreliable in corner cases (in particular, I never got keypresses like
Ctrl-C to cause the right things to happen in the hidden console), and
it involved some extremely intrusive refactoring of PuTTY itself (you
basically needed two totally different shapes of backend, one emitting
an escape-sequence stream fed to <code>terminal.c</code> and the other
directly providing updates to a rectangular character grid).

<p>
But in 2018, Windows itself helped out, by introducing the new <a
href="https://blogs.msdn.microsoft.com/commandline/2018/08/02/windows-command-line-introducing-the-windows-pseudo-console-conpty/">ConPTY</a>
interface to newer versions of Windows 10, which means now the
Windows API itself does the hard part, and all you have to do is to
interpret the escape-sequence stream you get back.

<p>
As of 2022-05, we're now building an experimental
<code>pterm.exe</code> for Windows. It's not part of the installer or
<code>putty.zip</code> yet, but it's linked from the Download page, so
you can download it and give it a try.

<p>
There's one known issue, involving bad handling of line wrapping on
Windows 10. We think that's actually a Windows issue: see <bug
id="win-pterm-line-wrap"/>.

<p>
If your version of Windows is too old for <code>pterm.exe</code> to
work at all, you'll see the error message <tt>Pseudo-console API is not
available on this Windows system</tt>.
