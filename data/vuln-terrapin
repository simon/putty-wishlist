Summary: Mitigations for SSH protocol 'Terrapin' vulnerability
Class: vulnerability
Priority: high
Difficulty: fun
Present-in: 0.79 7b10e34b8f636008243df1a1add61c0763befb38
Fixed-in: b80a41d386dbfa1b095c17bd2ed001477f302d46
Content-type: text/x-html-body

<p>
Security researchers at the Ruhr University Bochum have identified a
vulnerability in the SSH protocol (specifically, in widely-used
OpenSSH protocol extensions, exposed by weakness in the underlying SSH
protocol design), exploitable by an active attacker to break the
integrity of an encrypted SSH connection in specific ways. They have
named this the
<a href="https://terrapin-attack.com">'Terrapin' attack</a>,
and it has been assigned <cve id="CVE-2023-48795"/>.

<p>
PuTTY 0.80 will fully mitigate the protocol vulnerability when
connecting to a server which also has mitigations (such as
<a href="https://www.openssh.com/releasenotes.html#9.6">OpenSSH 9.6</a>),
or else will warn before proceeding with a connection where the attack
would be possible; either way, an attack can no longer be performed
without there being an opportunity to avoid the risk.

<hr>

<p>
Even in the absence of mitigations, the vulnerability can only be
exploited when certain cryptographic algorithms are in use. With
PuTTY's default cipher configuration and the most common server
configurations, connections would not be vulnerable in any case. (So
it's quite possible that users will not notice any difference with
PuTTY 0.80, even if connecting to a server with no mitigations for
this vulnerability.)

<p>
However, the Terrapin attack is possible if PuTTY's
<a href="https://the.earth.li/~sgtatham/putty/latest/htmldoc/Chapter4.html#config-ssh-encryption">cipher configuration</a>
has been changed from the default in a specific way, and/or the remote
server doesn't offer to use certain algorithms that aren't vulnerable
(either because it doesn't implement them, or has been configured not
to use them).

<p>
In more detail, the affected algorithms that PuTTY supports are:

<ul>
<li>
<b>ChaCha20-Poly1305</b> (which PuTTY calls 'ChaCha20').
<br>By default, PuTTY prefers the much more common AES family of
algorithms to ChaCha20, so if the server supports both, one of the AES
family will be used.
<br>However, if PuTTY has been reconfigured to prefer ChaCha20, then
that's likely to be used; or, if the server doesn't offer AES (apart
from AES-GCM), again, ChaCha20 is likely to be used. These are the
most likely scenarios for exposing the vulnerability.
</li>
<li>
<b>AES-CBC in encrypt-then-MAC ('ETM') mode</b>.
<br>While AES is in common use, and 'AES' is at the top of PuTTY's
default cipher preference list, PuTTY unconditionally prefers its CTR
mode to CBC, and the standard encrypt-after-MAC mode to
encrypt-then-MAC; this is not configurable. So the vulnerable mode of
AES can only be chosen if it's the only one the server offers (other
than AES-GCM), which would be unusual (especially as CTR mode was
specified for the SSH protocol before ETM was).
</li>
</ul>

<p>
To mitigate the vulnerability, the OpenSSH project has defined a SSH
extension called 'strict KEX' (documented in their 
<a href="https://cvsweb.openbsd.org/src/usr.bin/ssh/PROTOCOL?rev=HEAD&content-type=text/x-cvsweb-markup">PROTOCOL</a>
document), which PuTTY 0.80 implements.

<p>
When PuTTY &ge;0.80 connects to an upgraded server that implements
strict KEX, the vulnerability is avoided, regardless of the algorithms
in use. (This will require both PuTTY and the server to have been
upgraded after this vulnerability was made public.)
<br>(If strict KEX is in use, the message "Enabling strict key exchange
semantics" will be in PuTTY's Event Log.)

<p>
Otherwise, when PuTTY &ge;0.80 connects to a server without the
strict-KEX mitigation, <em>and</em> one of the vulnerable algorithms
mentioned above would be used for the connection, the PuTTY tools will
issue a warning similar to the following:

<blockquote>
<p>
The client-to-server cipher selected for this session is
ChaCha20-Poly1305, which, with this server, is vulnerable to the
'Terrapin' attack CVE-2023-48795, potentially allowing an attacker to
modify the encrypted session.

<p>
Upgrading, patching, or reconfiguring this SSH server is the best way
to avoid this vulnerability, if possible.

<p>
To accept the risk and continue, press "Yes". To abandon the
connection, press "No".
</blockquote>

<p>
"No" is the safest choice here.

<p>
(The name of the cipher, and the direction, could be different.)

<p>
If the server's offered algorithms are such that a simple change to
PuTTY's
<a href="https://the.earth.li/~sgtatham/putty/latest/htmldoc/Chapter4.html#config-ssh-encryption">cipher configuration</a>
can avoid the vulnerability, the warning will additionally advise what
exactly needs changing. (The SSH protocol design is such that the
cipher configuration must be changed before starting the connection.)

<p>
(We have not provided a way to suppress this warning, on the
assumption that it will be rare at worst to encounter a server with
which it's unavoidable, and in any case, such a server should soon be
upgraded or reconfigured now that the Terrapin attack is public
knowledge. However, if you find that there's some SSH server that it's
not practical to upgrade or reconfigure for some reason, and
reconfiguring PuTTY doesn't help, please let us know. If you believe
your need to connect to such a server outweighs the risk of attack,
you can always use an older version of PuTTY to connect to that
specific server in the short term; but also, let us know.)

<hr>

<p>
If the Terrapin attack is used to compromise the integrity of an SSH
connection, the attacker can only modify the traffic on the
supposedly-secure channel in a limited way ('prefix-truncation
attack'). The researchers' paper describes some specific kinds of
mischief that can be perpetrated as a result, but others may be
discovered. <em>We do not recommend proceeding past a warning of a
Terrapin-vulnerable SSH connection</em>, even if none of the
descriptions below worry you.

<p>
That said, here is how the main application-level attacks, all based
on the suppression of extensions reported via the <tt>ext-info</tt>
mechanism, affect PuTTY specifically:

<ul>
<li><b><tt>server-sig-algs</b></tt> is the only extension whose
suppression affects PuTTY. It allows the attacker to force use of the
weak hash SHA-1 in public-key user authentication. This does not
directly lead to a compromise, but could possibly allow a
well-resourced attacker to remove the meddler-in-the-middle protection
that public key authentication usually provides against an
intercepting proxy using an already-spoofed host key.
<br>(Or, with modern (&ge;8.8) OpenSSH in its default configuration,
cause a denial of service, since SHA-1 won't be accepted.)
</li>
<li><b><tt>publickey-hostbound@openssh.com</tt></b> makes no
difference to PuTTY/Pageant, since they do not implement host-bound
keys in the first place.
</li>
<li><b><tt>ping@openssh.com</tt></b> doesn't make any difference to
PuTTY either, since it doesn't implement the keystroke timing
obfuscation that this extension is used for.
</li>
</ul>

<p>
(The attacks against the AsyncSSH client described in the paper are
not relevant to the PuTTY tools.)

<hr>

<p>
This protocol vulnerability was pre-disclosed to us by Fabian Bäumer,
Marcus Brinkmann, and Jörg Schwenk, on 17 November 2023. For full
details of their report, see their
<a href="https://terrapin-attack.com">dedicated website about the Terrapin attack</a>.
