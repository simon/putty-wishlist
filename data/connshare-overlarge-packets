Summary: Pasting very long line into connection-sharing downstream can close it
Class: bug
Difficulty: fun
Priority: high
Present-in: 0.71
Fixed-in: 04b6db55f20b7057a5503c0a9f348bfba66b44f7
Content-type: text/x-html-body

<p>
In PuTTY's connection sharing system, the protocol that the ‘upstream’
and ‘downstream’ instances of PuTTY use to talk to each other has a
hard limit of just over 16Kb on the maximum size of a data packet that
may be sent.

<p>
This limit is enforced by the subsystem in the upstream instance that
acts as the server end of that protocol: if the upstream PuTTY
receives an overlarge packet, it will disconnect that downstream with
an error message.

<p>
But, embarrassingly, PuTTY's <em>own</em> client implementation of the
same protocol, spoken by a modified version of the ordinary SSH code
in the downstream instance, did not pay attention to that packet
limit. So in certain situations, it could send the upstream a packet
that exceeded the size limit, and get itself disconnected.

<p>
This can only be observed if the actual SSH server advertises a
maximum packet size greater than 16Kb. If the server's max packet size
is small enough, then PuTTY will not send overlarge packets for that
reason, so the bug will remain latent.

<p>
Even supposing the server does advertise a &gt;16Kb max packet, PuTTY
won't exceed it in typical interactive use. Even pasting a large block
of data into the terminal will often not exceed the packet size limit,
because the terminal code that handles pasting breaks the pasted data
up at line boundaries. So you would need to paste data with a
<em>single line</em> longer than 16Kb to trigger this bug in an
interactive session.
