Summary: Windows PuTTYgen's entropy collection worked badly with high-frequency mice
Class: bug
Priority: high
Difficulty: fun
Fixed-in: 5ad601ffcd5d09b5f22d998f71ce9ccdf35140f1
Content-type: text/x-html-body

<p>Windows PuTTYgen has always collected entropy for its key
generation by asking the user to waggle the mouse over the PuTTYgen
window, and collecting timing and coordinate data from the stream of
mouse movement events until it thinks it has enough randomness to
generate a key.

<p>However, PuTTYgen's method for calculating when it had enough
entropy did not take into account that some pointing devices (such as
gaming-oriented mice) send mouse movement events at an extremely high
rate like 1000 per second. If you were using a device like that,
PuTTYgen would <em>almost immediately</em> fill its entropy buffer and
start generating a key, almost as soon as the mouse twitched once.

<p>This very likely led to PuTTYgen collecting less entropy than it
should have, because in that initial tiny mouse twitch, all the
movement events would have been pointing in the same direction and
going at very nearly the same speed. There's no time for the user to
wave their hand back and forth to introduce variation.

<p>For 0.77, we've introduced a rate limit, so that PuTTY will no
longer assume that a mouse event has the same entropy no matter how
quickly they're arriving. Instead, it has an upper bound on the amount
of entropy it thinks it can receive per unit time, so that even users
with gaming mice will have to wave the mouse back and forth in a way
that actually generates randomness.

<p>(However, this is only one of PuTTYgen's entropy sources. We also
use Windows's <code>CryptGenRandom</code> to supplement that
mouse-based entropy collection. So keys generated by the previous
PuTTYgen are not necessarily compromised.)
