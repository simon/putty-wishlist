Summary: pterm.exe handles line breaks badly on Windows 10
Class: semi-bug
Difficulty: taxing
Present-in: 0.77
Content-type: text/x-html-body

<p><code>pterm.exe</code> on Windows 10 has some bad behaviour around
line breaks and line wrapping.

<p>If text wraps from one line to the next, and you copy and paste
across the line break, <code>pterm</code> will include a physical line
break in the pasted data, where the real Windows command prompt would
not.

<p>Also, if you type a line at the <code>cmd.exe</code> prompt that's
long enough to wrap on to a second line, and then delete characters
until the cursor moves back over the line break, a display glitch
occurs which leaves the cursor off by one character from where
<code>cmd.exe</code> thinks it is.

<p>These <em>look</em> like bugs in <code>pterm.exe</code>, but we
think they're actually bugs in the Windows component that translates
console devices into escape sequences. In particular, on affected
systems, <code>pterm.exe</code> receives <em>exactly the same</em>
stream of input data from Windows when text wraps across a line break
as it does if the same text is displayed with a deliberate newline.
(So it can't treat the two situations differently, because they look
identical to it.)

<p>When we tested on Windows 11, these bugs didn't occur, which also
supports the theory that a Windows component was causing them. It
looks as if the version of that console translation component in
Windows 11 has had these bugs fixed. So there's probably nothing we
can do about them on Windows 10. Sorry.

<p>(We think at least one of these issues might be related to
<a href="https://github.com/microsoft/terminal/issues/1245">issue #1245</a>
in MS's Github repository for the terminal-handling code.)
