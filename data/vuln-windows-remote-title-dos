Summary: Rapid changes of window title can DoS Windows GUI
Class: vulnerability
Priority: high
Difficulty: tricky
Present-in: 0.74
Fixed-in: d74308e90e3813af664f91ef8c9d1a0644aa9544
Content-type: text/x-html-body

<p>In the Windows version of PuTTY (and PuTTYtel), if the server sent
a rapid series of terminal escape sequences that repeatedly changed
the title of the terminal window, the Windows GUI could become
unresponsive, because it couldn't keep up with all the title changes.

<p>We think this is primarily a bug in Windows itself. (If it can't
handle window title changes that quickly, it should apply a rate
limit, or else a buggy local program can cause the same problem, by
accident or on purpose.)

<p>But PuTTY's feature of allowing server-controlled title changes had
the effect of exporting that local bug over the network, and turning
it into a <em>remotely</em> triggerable denial of service: it permits
a server, or a server-side application with the ability to write to
your terminal device, to DoS the client machine's Windows GUI.

<p>In 0.75, the terminal code has been reworked so that title changes
are buffered within PuTTY itself, and a rate limit is applied before
they are passed on to the Windows API. So if the server changes the
window title 10000 times in a second, only 50 of the title changes
will actually get as far as the Windows API; the rest will just keep
rewriting a buffer inside PuTTY's terminal emulation code.

<p>For previous versions of PuTTY, an easy workaround is to disable
the feature that allows the server to set the window title: on the
Features panel, tick the box "Disable remote-controlled window title
changing".

<p>This vulnerability was discovered by Eviatar Gerzi of CyberArk Labs.
It has been assigned <cve id="CVE-2021-33500"/>.
