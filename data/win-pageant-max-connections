Class: bug
Summary: After 30 or so simultaneous PuTTY sessions, Windows Pageant stops working
Difficulty: fun
Priority: high
Present-in: 0.75
Fixed-in: 6e69223dc262755c2cd2a3bba5c188d8fc91943a 11b89407f5d225e461a266a9c7f23299e5314abf
Content-type: text/x-html-body

<p>
In version 0.75, Windows Pageant would stop working after a certain
number of PuTTYs (or other PuTTY tools) were active at the same time
(typically about 30). Subsequently started PuTTY tools would tend to
hang.

<p>
This was due to two separate bugs in the new Pageant connection
arrangements introduced in 0.75 (<bug id="pageant-named-pipe"/>):
<ul>
<li>Whenever PuTTY made a named-pipe connection to Pageant, it would
not close the connection when it was done with it. This led to connections
used for initial authentication staying needlessly open for the
lifetime of the PuTTY tool. This leak was fixed on the <tt>main</tt>
branch in commit <gitcommit hash="6e69223dc262755c2cd2a3bba5c188d8fc91943a"/>, which fixed the most obvious manifestations
of this problem.</li>
<li>That there was a limit at all on simultaneous connections to
Pageant was fixed later, in commit <gitcommit
hash="17c57e1078c8fabd5417175247b38ce7b1643ca4"/>: see <bug
id="win-pageant-max-connections-2"/>. (The limit was about 60, but
each PuTTY tool might make more than one.)</li>
</ul>
