Summary: Spurious application manifests caused CLI tools to fail on XP
Class: bug
Absent-in: 0.81
Present-in: 0.82
Fixed-in: 27550b02e26c71a9638ff25aeaeff32183ca5a3f
Content-type: text/x-html-body

<p>The command-line tools in the PuTTY suite, as of version 0.82,
didn't run at all on Windows XP, giving an unhelpfully vague error
message of the form

<blockquote>
The application has failed to start because the application
configuration is incorrect. Reinstalling the application may fix this problem.
</blockquote>

<p>This wasn't intentional on our part: we didn't deliberately
withdraw support for the standard builds of PuTTY running on XP
(although as I understand it XP is no longer itself in security
support by Microsoft). It happened because a build-tools upgrade
inserted a default application manifest into those files, which XP was
unhappy with the formatting of (for similar reasons to the much older
bug <bug id="xp-wont-run"/>). Now those files have no manifest at all,
which is the same way they were in 0.81 and before.
