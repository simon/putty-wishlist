Summary: Windows Vista update KB3057839 breaks PuTTY configuration dialog
Class: bug
Priority: high
Present-in: 0.52 0.53b 0.54 0.58 0.60 0.63 0.64 2015-06-09
Fixed-in: 2015-06-19 6163710f043fb58fc80f6b45c14a92f7036bde75 0.65
Content-type: text/x-html-body

<p>
Many people have reported that on Windows Vista (but not Windows 7),
after the Windows security update
<a href="https://support.microsoft.com/en-us/kb/3057839">KB3057839</a>
(part of the update
<a href="https://technet.microsoft.com/en-us/library/security/ms15-061.aspx">MS15-061</a>
published on 9 June 2015), the PuTTY configuration window does not
display. Only a taskbar icon is visible. The window is in fact present,
but invisible; keyboard input and even blind clicking on the invisible
configuration window still work.
(Some public reports:
<a href="http://www.geekzone.co.nz/forums.asp?forumid=45&topicid=174953">one</a>,
<a href="http://superuser.com/questions/927268/cant-see-putty-window-on-windows-vista-any-longer">two</a>,
<a href="http://www.vistaforums.com/putty-dont-start-since-couple-days-t96280.html">three</a>.)

<p>
The PuTTY terminal window works when launched from the command line
(<tt>putty -load sessionname</tt>, <tt>putty -ssh hostname.full.domain
-l username</tt>). Other PuTTY suite programs such as PuTTYgen work.

<p>
Uninstalling the KB3057839 update is reported to restore PuTTY to a
working state; however, since that's a security update, that's not an
ideal solution. (One correspondent reported that uninstalling
KB3058515 also worked.)

<p>
We
<a href="http://answers.microsoft.com/en-us/windows/forum/windows_vista-windows_programs/windows-update-ms15-061-kb3057839-breaks-putty/f5d4c549-59ff-4a17-bff5-2d4c810be2dd?tm=1434100111773">asked on Microsoft's forum</a>;
this has yielded another program
(<a href="https://forum.dbpoweramp.com/showthread.php?36159-CD-Ripper-and-Batch-Converter-No-Longer-Show-in-Windows-Vista-after-Security-Update">dBpoweramp</a>)
suffering from what sounds like the same symptom.

<p>
One correspondent's experience suggests that the window is present but
invisible (transparent); they've
<a href="http://superuser.com/a/927318">also posted publicly</a>.
They can restore visibility with an
<a href="http://ahkscript.org/">AutoHotKey</a> script containing the
following:

<pre>WinWait, PuTTY Configuration
WinSet, Transparent, 255</pre>

<p>
It's also been reported that hiding and showing the window with
<tt>ShowWindow()</tt> (by hacking the PuTTY source, or externally
from PowerShell) makes the window reappear.

<p>
Another perturbation that makes the symptom go away is mucking around
with <tt>SetWindowLong()</tt>. It's not clear why, but it has made us
a bit suspicious of our use of that family of functions.

<p>
Another correspondent reports that setting the Windows properties
'Disable desktop composition' or 'Run in 256 colors' on putty.exe (in
the Compatibility tab of the file properties?) avoids the issue.
Others report that disabling desktop effects entirely (by selecting
the "Windows Vista Basic" colour scheme) also works, although that's
a sledgehammer to crack a nut. Presumably all these methods work by
removing the possibility of transparency.

<p>
None of PuTTY's settings appear to have any effect on this behaviour
(which is as you'd expect, since the configuration dialog is itself
not configurable); at least, many people have tried erasing their
saved sessions, uninstalling, using <tt>-cleanup</tt> etc with no
improvement.

<p>
<em>Update, 2015 Jun 18:</em> we think we've found the root cause
of this. It was apparently only by luck that the configuration dialog
appeared at all, on any version of Windows (gory details in the
<gitcommit hash="6163710f043fb58fc80f6b45c14a92f7036bde75">commit message</gitcommit>).

<p>
The bug we've found is fixed in development snapshots dated 2015-06-19
or later. It made the symptom go away on our test machine, and on those
of all who originally raised the issue and reported back to us after
testing the update.
