Summary: Command-line option -pwfile to read a password from a file
Class: wish
Fixed-in: 44ee7b9e765f7af80d94610c82dbad46c5932ca0
Content-type: text/x-html-body

<p>When you use the <code>-pw</code> command-line option to provide a
password on the command line, your password is visible to anyone who
can see the command line itself (e.g. via the Windows task manager,
which can show full command lines with the right configuration).

<p>Now all the tools also support <code>-pwfile</code> as an
alternative. Its argument is the name of a text file to read the
password from, instead of the password itself. So somebody reading the
command line will only learn <em>where</em> your password is kept, not
what it is.

<p>We still don't recommend the use of either of these options, in any
circumstance where the password actually matters to you. But if you
really have no other options, <code>-pwfile</code> is one notch safer
than <code>-pw</code>.
