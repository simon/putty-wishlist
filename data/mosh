Summary: Compatibility with Mosh (mobile shell)
Class: wish
Priority: low
Difficulty: taxing
Content-type: text/x-html-body

<p>Several people have asked us to extend PuTTY to add compatibility
with <a href="https://mosh.mit.edu/">Mosh</a>, a rather unusual
remote-terminal application that's particularly suited to use over bad
networks.  This is a tempting idea in principle, but there are several
practical problems:

<ul>
<li>The State Synchronization Protocol used by Mosh doesn't seem to
have any public documentation.  This makes writing another
implementation difficult.

<li>Unlike all the protocols currently supported by PuTTY, Mosh
doesn't transport a terminal stream but updates the screen by other
means, so it would need hooks into the middle of the current
monolithic terminal emulator.  That would be a fairly large amount of
work.

<li>Similarly, Mosh's use of the network is very different from all
our existing backends, which use small numbers of TCP connections.

<li>On the other hand, Mosh does require an SSH client implementation
for authentication and key-exchange, so there are some major parts of
PuTTY that could be re-used.

<li>Mosh uses OCB, which is patented in the USA.  While there are <a
href="http://web.cs.ucdavis.edu/~rogaway/ocb/license.htm">fairly
generous licences</a> available for the relevant patents, these are
not as generous as PuTTY's copyright licence, so they would forbid
uses of PuTTY's code that are currently allowed (specifically in
proprietary software in military use).
</ul>

<p>It's possible that someone could write a patch to add Mosh
compatibility to PuTTY that we'd be willing to accept, but it's not
the most likely thing ever.
