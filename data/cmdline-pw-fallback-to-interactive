Summary: Unintended fallback to interactive password prompt if -pw fails
Class: bug
Difficulty: fun
Fixed-in: a82ab70b0bf418a7c18d07e0090bbf194f795cbe
Content-type: text/x-html-body

<p>If you specify a login password on PuTTY's command line, and the
server rejects that password, the intended behaviour is that PuTTY
abandons the attempt at password login completely.

<p>In fact, before 0.77, what would happen was that PuTTY would fall
back to prompting interactively for a password, as if the one provided
on the command line had been a mistyped first attempt.

<p>This wasn't the intended behaviour, so we've restored it to the
original intention. So if you have obsolete passwords in command
lines, they will now cause login failure rather than a fallback to an
interactive prompt.

<p>This applies to both the deprecated <code>-pw</code> option and the
<bug id="cmdline-password-file">newly introduced</a> <code>-pwfile</code>.
