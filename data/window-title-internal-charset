Summary: Can't set window title in ISO 8859 character sets and a few others
Class: bug
Difficulty: fun
Absent-in: 4f41bc04ab27953cce112070b796b52a8d0de52d 0.76
Present-in: c35d8b832801d926a16ad29c416b969654051ef0 0.77
Fixed-in: a216d86106d40c38f05f1ffc03996be54d590aa6
Content-type: text/x-html-body

<p>As of PuTTY 0.77, the escape sequences that set the terminal window
title are interpreted according to the currently configured character
set. (Previously, they were interpreted according to the OS's default
character set, which was not what the server sending such a sequence
was expecting; see <bug id="window-title-charset"/>.)

<p>Unfortunately, on Windows, this introduced a bug in which for
<em>some</em> single-byte character sets, such as the ISO 8859 family,
the character set conversion would fail, and the window title would
end up set to the empty string instead of the intended text.

<p>This bug affects the character sets that PuTTY implements
internally using built-in translation tables:
<ul>
<li>the whole ISO 8859 family
<li>KOI8-U
<li>HP-ROMAN8
<li>VSCII
<li>the DEC Multinational Character Set
</ul>

<p>Character sets that PuTTY handles using the Windows API, such as
Win1252 and KOI8-R, are unaffected. UTF-8 is also unaffected.
