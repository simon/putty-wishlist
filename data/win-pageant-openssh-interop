Summary: Make it easy for Windows OpenSSH to talk to Windows Pageant 
Class: wish
Difficulty: fun
Fixed-in: 8a2883933d4f664681a015341cf8760a0231a41b
Content-type: text/x-html-body

<p>In version 0.75, Windows Pageant <bug
id="pageant-named-pipe">introduced a new transport</bug> for clients
to talk to the SSH agent. This transport uses the Windows 'named pipe'
system.

<p>Modern Windows now provides a version of OpenSSH as an optional
Windows component (installable from the Windows settings user
interface). That too uses named pipes as a transport for its agent,
and the protocol spoken over the pipes is identical to Pageant's. (Not
surprisingly, since there's only one sensible choice.)

<p>So it's possible to make Windows's <code>ssh.exe</code> use Pageant
as its agent, if you configure OpenSSH to know the pathname of
Pageant's named pipe. However, Pageant's pipes are named in a
complicated cryptographic way, to make it harder for the wrong user to
squat on them (since Windows doesn't provide per-user protected
subspaces of the named pipe namespace). So this is tricky to set up.

<p>For 0.77, we've introduced a convenience feature that makes it less
tricky. With an extra command-line option
<code>--openssh-config</code>, Pageant can now write out a snippet of
OpenSSH configuration, which you can refer to from your main
<code>.ssh\config</code> file using an <code>Include</code> directive.

<p>(Note, however, that this only applies to the version of OpenSSH
provided as a Windows component. Not all things called
<code>ssh.exe</code> on Windows are the same as each other. In
particular, a completely different port of OpenSSH is provided by the
Windows Git installation, which doesn't use named pipes, and won't be
able to understand this configuration snippet.)
