Summary: Support OpenSSH 7.6+ aes256-ctr-encypted private keys
Class: wish
Difficulty: fun
Fixed-in: 044a9304e3215c1933f6afde35aab594d8631639
Content-type: text/x-html-body

<p>
As of <a href="http://www.openssh.com/releasenotes.html#7.6">OpenSSH 7.6</a>,
ssh-keygen produces its new-format private keys encoded with the
aes256-ctr cipher, instead of aesctr-cbc.
(<a href="https://bugzilla.mindrot.org/show_bug.cgi?id=2754">bz#2754</a>)

<p>
PuTTYgen needs to understand this new (to us) format variant when
importing private keys. In 0.70 it says "unrecognised cipher name".

<p>
Also, we changed to outputting new-style OpenSSH keys with this
cipher mode, unconditionally. (Older versions of OpenSSH seem to
understand aes256-ctr encrypted keys, so this doesn't seem to be an
interoperability problem.)
